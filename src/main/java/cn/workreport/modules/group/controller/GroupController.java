package cn.workreport.modules.group.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.group.entity.Group;
import cn.workreport.modules.group.service.IGroupService;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.report.po.ReportCommitPO;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 组织架构表 前端控制器
 * </p>
 *
 * @author yyf
 * @since 2022-02-22
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "组织模块")
public class GroupController extends BaseController {

    @Autowired
    private IGroupService groupService;

    /**
     * 保存或修改组织架构表
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存组织架构表")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_SAVE_OR_UPDATE})
    @PostMapping("/group/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(Group entity) {
        log.info("===> 进入 /group/saveOrUpdate ===> params = " + entity);
        return groupService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询组织架构表
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询组织架构表")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_PAGE})
    @PostMapping("/group/listPage")
    public JsonResult<?> listPage(PagePO<Group> pagePO) {
        log.info("===> 进入 /group/listPage ===> params = " + pagePO);
        IPage<Group> page = new PageQueryUtil<Group>().getPage(pagePO);
        LambdaQueryWrapper<Group> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Group::getCreatedTime);
        PageVO<Group> result = groupService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询组织架构表
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询组织架构表")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_PAGE})
    @GetMapping("/group/getById")
    public JsonResult<?> getById(Integer id) {
        log.info("===> 进入 /group/getById ===> params = " + id);
        // 暂时未用到
        return JsonResult.ok();
    }

    /**
     * 批量或单体删除组织架构表
     *
     * @param idsPO ID集合
     * @return 成功与否
     */
    @ApiOperation("批量删除组织架构表")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_DELETE})
    @PostMapping("/group/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO) {
        log.info("===> 进入 /group/removeByIds ===> params = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO)) {
            return JsonResult.fail("ids 不能为空！");
        }
        return groupService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 获取树形数据
     *
     * @param orgId 机构id
     * @return 查询结果
     */
    @ApiOperation("获取树形数据")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_PAGE})
    @GetMapping("/group/treeList")
    public JsonResult<?> treeList(Integer orgId) {
        log.info("===> 进入 /group/treeList ===>");
        // 暂时未用到
        return groupService.getTreeListAll(orgId);
    }

    /**
     * 查询自己组和下级的成员发送的日报记录
     *
     * @param reportCommitPO reportCommitPO
     * @return JsonResult
     */
    @ApiOperation("查询自己组和下级的成员发送的日报记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_REPORT_TREE})
    @PostMapping("/group/groupReportTree")
    public JsonResult<?> groupReportTree(@RequestBody ReportCommitPO reportCommitPO) {
        log.info("===> 进入 /group/groupReportTree ===> params: reportCommitPO=" + reportCommitPO);
        return groupService.groupReportTree(reportCommitPO.getStartCreatedTime(), reportCommitPO.getEndCreatedTime(), reportCommitPO.getOrgId());
    }

    /**
     * 查询自己组的成员发送的日报记录
     *
     * @param reportCommitPO reportCommitPO
     * @return JsonResult
     */
    @ApiOperation("查询自己组的成员发送的日报记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.GROUP_REPORT_TREE})
    @PostMapping("/group/memberReport")
    public JsonResult<?> memberReport(@RequestBody ReportCommitPO reportCommitPO) {
        log.info("===> 进入 /group/memberReport ===> params: reportCommitPO=" + reportCommitPO);
        List<Group> groupList = groupService.memberReport(reportCommitPO.getStartCreatedTime(), reportCommitPO.getEndCreatedTime());
        return JsonResult.ok(groupList);
    }

    /**
     * 根据机构id查询该机构所有组织
     *
     * @param orgId 机构id
     * @return JsonResult
     */
    @ApiOperation("根据机构id查询该机构所有组织")
//    @UserLoginToken(permission = { PermissionEnum.GROUP_REPORT_TREE })
    @GetMapping("/group/listGroupByOrgId")
    public JsonResult<?> listGroupByOrgId(Integer orgId) {
        log.info("===> 进入 /group/listGroupByOrgId ===> params: orgId=" + orgId);
        if (ObjectUtil.isEmpty(orgId)) {
            return JsonResult.fail("参数 orgId 不能为空");
        }
        List<Group> groupList = groupService.listGroupByOrgId(orgId);
        return JsonResult.ok(groupList);
    }

}
