package cn.workreport.modules.group.mapper;

import cn.workreport.modules.group.entity.Group;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 组织架构表 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-22
 */
@Mapper
public interface GroupMapper extends BaseMapper<Group> {

}
