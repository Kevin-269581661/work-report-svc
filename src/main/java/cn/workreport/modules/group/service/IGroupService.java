package cn.workreport.modules.group.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.group.entity.Group;
import cn.workreport.util.JsonResult;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 组织架构表 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-22
 */
public interface IGroupService extends BaseService<Group> {

    /**
     * 获取组织结构的树形数据
     *
     * @param orgId 机构id
     * @return JsonResult
     */
    JsonResult<?> getTreeListAll(Integer orgId);

    /**
     * 查询自己组和下级的成员发送的日报记录
     *
     * @param startCreatedTime 开始时间
     * @param endCreatedTime   结束时间
     * @return JsonResult
     */
    JsonResult<?> groupReportTree(Date startCreatedTime, Date endCreatedTime, Integer orgId);

    /**
     * 查询自己组的成员发送的日报记录
     *
     * @param startCreatedTime 开始时间
     * @param endCreatedTime   结束时间
     * @return JsonResult
     */
    List<Group> memberReport(Date startCreatedTime, Date endCreatedTime);

    /**
     * 根据机构id查询该机构所有组织
     *
     * @param orgId 机构id
     * @return JsonResult
     */
    List<Group> listGroupByOrgId(Integer orgId);
}

