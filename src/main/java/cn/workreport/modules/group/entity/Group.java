package cn.workreport.modules.group.entity;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.workreport.modules.common.entity.BaseEntity;
import cn.workreport.modules.report.dto.UserReportCommitDTO;
import cn.workreport.modules.users.vo.CommonUserVO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 组织架构表
 * </p>
 *
 * @author yyf
 * @since 2022-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_group")
@ApiModel(value = "Group对象", description = "组织架构表")
public class Group extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField
    @ApiModelProperty("组织名称")
    private String groupName;

    @TableField
    @ApiModelProperty("父级的id")
    private Integer parentId;

    @TableField
    @ApiModelProperty("排序编号 以10位为一个阶梯")
    private Integer orderNumber;

    @TableField
    @ApiModelProperty("机构id")
    private Integer orgId;

    //--------------------------------------以下为非表字段--------------------------------------------------
    @TableField(exist = false)
    private List<Group> children;

    @TableField(exist = false)
    private Integer level;

    @TableField(exist = false)
    private List<CommonUserVO> members;

    @TableField(exist = false)
    private List<UserReportCommitDTO> userReportCommitList;


//--------------------------------------以上为非表字段---------------------------------------------
}
