package cn.workreport.modules.permission.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.permission.entity.Permission;
import cn.workreport.modules.permission.po.PermissionPO;
import cn.workreport.util.JsonResult;

import java.util.List;

/**
 * <p>
 *  业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
public interface IPermissionService extends BaseService<Permission> {

    /**
     * 根据角色id 查询权限列表
     * @return
     */
    List<Permission> getPermissionListByRoleId(Integer roleId);

    /**
     * 预加载所有的权限列表
     * @return
     */
    JsonResult<?> getPermissionAll();

    /**
     * 给角色批量添加权限
     * @return
     */
    JsonResult<?> saveRolePermission(PermissionPO permissionPO);
}

