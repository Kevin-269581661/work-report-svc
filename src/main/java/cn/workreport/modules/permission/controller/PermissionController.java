package cn.workreport.modules.permission.controller;

import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.permission.po.PermissionPO;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import cn.workreport.modules.permission.entity.Permission;
import cn.workreport.modules.permission.service.IPermissionService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "权限模块")
public class PermissionController extends BaseController {

    @Autowired
    private IPermissionService permissionService;

    /**
     * 保存或修改
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存")
    @UserLoginToken(required = true, permission = { PermissionEnum.PERMISSION_SAVE_OR_UPDATE })
    @PostMapping("/permission/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(Permission entity) {
        log.info("===> 进入 /permission/saveOrUpdate ===> params = " + entity);
        return permissionService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询")
    @UserLoginToken(required = true, permission = { PermissionEnum.PERMISSION_PAGE })
    @PostMapping("/permission/listPage")
    public JsonResult<?> listPage(PagePO<Permission> pagePO){
        log.info("===> 进入 /permission/listPage ===> params = " + pagePO);
        IPage<Permission> page = new PageQueryUtil<Permission>().getPage(pagePO);
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Permission::getCreatedTime);
        PageVO<Permission> result = permissionService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询
     * 如果查询不到，new 一个返回，可以作为新建请求
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询")
    @UserLoginToken(required = true, permission = { PermissionEnum.PERMISSION_PAGE })
    @GetMapping("/permission/getById")
    public JsonResult<?> getById(Integer id){
        log.info("===> 进入 /permission/getById ===> params = " + id);
        // 暂时未用到
        return JsonResult.ok();
    }

    /**
    * 批量或单体删除
    *
    * @param idsPO ID集合
    * @return 成功与否
    */
    @ApiOperation("批量删除")
    @UserLoginToken(required = true, permission = { PermissionEnum.PERMISSION_DELETE })
    @PostMapping("/permission/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO){
        log.info("===> 进入 /permission/removeByIds ===> params = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO)) {
            return JsonResult.fail("ids 不能为空！");
        }
        return permissionService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 给角色批量添加权限
     *
     * @param permissionPO
     * @return
     */
//    @ApiOperation("给角色批量添加权限")
//    @PostMapping("/permission/saveRolePermission")
//    public JsonResult<?> saveRolePermission(PermissionPO permissionPO){
//        log.info("===> 进入 /permission/saveRolePermission ===> params = " + permissionPO);
//        return permissionService.saveRolePermission(permissionPO);
//    }

    /**
     * 预加载所有的权限列表
     * @return
     */
     @ApiOperation("预加载所有的权限列表")
     @GetMapping("/permission/getPermissionAll")
     public JsonResult<?> getPermissionAll() {
       return permissionService.getPermissionAll();
     }

}
