package cn.workreport.modules.permission.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_role_permission")
@ApiModel(value = "Permission对象", description = "")
public class Permission extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField
    @ApiModelProperty("角色id")
    private Integer roleId;

    @TableField
    @ApiModelProperty("角色权限")
    private String permission;

//--------------------------------------以下为非表字段--------------------------------------------------

//--------------------------------------以上为非表字段---------------------------------------------
}
