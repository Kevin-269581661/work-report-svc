package cn.workreport.modules.permission.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 权限模块
 */
@AllArgsConstructor
public enum PermissionModuleEnum implements ParamEnum {

    /**
     *  用户模块
     */
    USERS("users", "用户模块"),

    /**
     * 任务模块
     */
    TASK("task", "任务模块"),

    /**
     * 版本模块
     */
    RELEASE("release", "版本模块"),

    /**
     * 角色模块
     */
    ROLE("role", "角色模块"),

    /**
     * 日报模块
     */
    REPORT("report", "日报模块"),

    /**
     * 权限模块
     */
    PERMISSION("permission", "权限模块"),

    /**
     * 组织架构模块
     */
    GROUP("group", "组织架构模块"),

    /**
     * 项目级联模块
     */
    CASCADE("cascade", "项目级联模块"),

    /**
     * 机构模块
     */
    ORGANIZATION("organization", "机构模块");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static PermissionModuleEnum getEntityByVal (String value) {
        for (PermissionModuleEnum entity : PermissionModuleEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
