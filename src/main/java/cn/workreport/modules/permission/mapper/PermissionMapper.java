package cn.workreport.modules.permission.mapper;

import cn.workreport.modules.permission.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

}
