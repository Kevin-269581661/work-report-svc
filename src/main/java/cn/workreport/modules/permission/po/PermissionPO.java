package cn.workreport.modules.permission.po;

import lombok.Data;

import java.util.List;

@Data
public class PermissionPO {

    private Integer roleId;

    private List<String> permissionList;
}
