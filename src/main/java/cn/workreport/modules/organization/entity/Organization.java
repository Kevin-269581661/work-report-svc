package cn.workreport.modules.organization.entity;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.organization.enums.OrgStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 机构对应实体类
 * </p>
 *
 * @author yyf
 * @since 2022-03-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_organization")
@ApiModel(value = "Organization", description = "机构对应实体类")
public class Organization extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("机构名称")
    @TableField
    private String orgName;

    @ApiModelProperty("机构简称")
    @TableField
    private String orgShortName;

    @ApiModelProperty("机构状态（1：启用，2：停用）")
    @TableField
    private OrgStatusEnum orgStatus;

    @ApiModelProperty("机构描述")
    @TableField
    private String orgDesc;

    @ApiModelProperty("机构logo")
    @TableField
    private String orgAvatar;

    @ApiModelProperty("机构管理员id")
    @TableField
    private Integer orgManagerId;

    @ApiModelProperty("是否可加入")
    @TableField
    private IsEnum canJoin;

    @ApiModelProperty("是否开启验证（枚举）")
    @TableField
    private IsEnum isOpenVerification;

    @ApiModelProperty("验证的问题")
    @TableField
    private String verifyQuestion;

    @ApiModelProperty("验证的答案")
    @TableField
    private String verifyAnswer;

//--------------------------------------以下为非表字段--------------------------------------------------
    @TableField(exist = false)
    private String orgStatusLabel;

    @TableField(exist = false)
    private String canJoinLabel;

    /**
     * 机构管理员名称(取登录名)
     */
    @TableField(exist = false)
    private String orgManagerName;

    /**
     * 人员数量
     */
    @TableField(exist = false)
    private Integer memberCount;
//--------------------------------------以上为非表字段---------------------------------------------
}
