package cn.workreport.modules.organization.mapper;

import cn.workreport.modules.organization.entity.Organization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 机构 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-03-24
 */
@Mapper
public interface OrganizationMapper extends BaseMapper<Organization> {

}
