package cn.workreport.modules.organization.service;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.organization.entity.Organization;
import cn.workreport.modules.organization.enums.OrgStatusEnum;
import cn.workreport.util.JsonResult;

/**
 * <p>
 * 机构 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-03-24
 */
public interface IOrganizationService extends BaseService<Organization> {

    /**
     * 查询预加载数据
     * @return
     */
    JsonResult<?> preparedData();

    /**
     * 设置机构停用/启用
     * @param id
     * @param status
     * @return
     */
    JsonResult<?> setStatus(Integer id, OrgStatusEnum status);

    /**
     * 设置机构是否可加入
     *
     * @param orgId
     * @param canJoin
     * @return
     */
    JsonResult<?> setCanJoin(Integer orgId, IsEnum canJoin);
}

