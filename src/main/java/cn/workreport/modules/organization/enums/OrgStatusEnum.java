package cn.workreport.modules.organization.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 机构状态枚举
 */
@AllArgsConstructor
public enum OrgStatusEnum implements ParamEnum {

    /**
     * 启用
     */
    ENABLE("1", "启用"),

    /**
     * 停用
     */
    DISABLED("0", "停用");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static OrgStatusEnum getEntityByVal (String value) {
        for (OrgStatusEnum entity : OrgStatusEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
