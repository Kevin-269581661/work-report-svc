package cn.workreport.modules.task.mapper;

import cn.workreport.modules.task.entity.TaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author yyf
 * @email sunlightcs@gmail.com
 * @date 2021-11-10 13:04:34
 */
@Mapper
public interface TaskMapper extends BaseMapper<TaskEntity> {

}
