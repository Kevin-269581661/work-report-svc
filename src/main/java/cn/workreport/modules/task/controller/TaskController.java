package cn.workreport.modules.task.controller;

import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.task.entity.TaskEntity;
import cn.workreport.modules.task.service.ITaskService;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * 任务模块
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "任务模块")
public class TaskController extends BaseController {
    @Autowired
    private ITaskService taskService;

    /**
     * 保存或修改任务
     */
    @ApiOperation("保存或修改任务")
    @UserLoginToken(required = true, permission = { PermissionEnum.TASK_SAVE_OR_UPDATE })
    @PostMapping("/task/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(TaskEntity taskEntity){
        log.info("===> 进入 /task/saveOrUpdate ===> params = " + taskEntity);
        return taskService.saveOrUpdateEntity(taskEntity);
    }

    /**
     * 分页查询列表任务
     */
    @ApiOperation("分页查询列表任务")
    @UserLoginToken(required = true, permission = { PermissionEnum.TASK_PAGE })
    @PostMapping("/task/listPage")
    public JsonResult<?> listPage(@RequestBody PagePO<TaskEntity> pagePO){
        log.info("===> 进入 /task/listPage ===> params = " + pagePO);
        IPage<TaskEntity> page = new PageQueryUtil<TaskEntity>().getPage(pagePO);
        LambdaQueryWrapper<TaskEntity> wrapper = new LambdaQueryWrapper<>();
        TaskEntity condition = pagePO.getCondition();
        if (!ObjectUtils.isEmpty(condition)) {
            wrapper.eq(!ObjectUtils.isEmpty(condition.getReleaseId()), TaskEntity::getReleaseId, condition.getReleaseId());
            wrapper.eq(!ObjectUtils.isEmpty(condition.getTaskStatus()), TaskEntity::getTaskStatus, condition.getTaskStatus());
            wrapper.eq(!ObjectUtils.isEmpty(condition.getTaskPriority()), TaskEntity::getTaskPriority, condition.getTaskPriority());
        }
        wrapper.orderByDesc(TaskEntity::getCreatedTime);
        PageVO<TaskEntity> result = taskService.pageEntity(page, wrapper);
        if (!ObjectUtils.isEmpty(result)) {
            result.getList().forEach(taskEntity -> taskService.wrapEntity(taskEntity));
        }
        return JsonResult.ok(result);
    }

    /**
     * 根据id获取任务详情
     */
    @ApiOperation("根据id获取任务详情")
    @UserLoginToken(required = true, permission = { PermissionEnum.TASK_PAGE })
    @GetMapping("/task/getById")
    public JsonResult<?> getById(Integer id){
        // 暂时未用到
        return JsonResult.ok();
    }

    /**
     * 删除任务
     */
    @ApiOperation("删除任务")
    @UserLoginToken(required = true, permission = { PermissionEnum.TASK_DELETE })
    @PostMapping("/task/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO){
        if (ObjectUtils.isEmpty(idsPO)) {
            return JsonResult.fail("ids 不能为空！");
        }
        return taskService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 获取任务预加载数据
     * @return
     */
    @ApiOperation("获取任务预加载数据")
    @GetMapping("/task/preparedData")
    public JsonResult preparedData() {
        return taskService.preparedData();
    }

}
