package cn.workreport.modules.task.controller;

import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.task.entity.ReleaseEntity;
import cn.workreport.modules.task.service.IReleaseService;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * 版本模块
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "任务版本模块")
public class ReleaseController extends BaseController {
    @Autowired
    private IReleaseService releaseService;

    /**
     * 保存或修改版本
     */
    @ApiOperation("保存或修改版本")
    @UserLoginToken(required = true, permission = { PermissionEnum.RELEASE_SAVE_OR_UPDATE })
    @PostMapping("/release/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(ReleaseEntity releaseParams){
        log.info("===> 进入 /release/saveOrUpdate ===> params = " + releaseParams);
        return releaseService.saveOrUpdateEntity(releaseParams);
    }

    /**
     * 查询列表版本
     */
    @ApiOperation("分页查询版本")
    @UserLoginToken(required = true, permission = { PermissionEnum.RELEASE_PAGE })
    @PostMapping("/release/listPage")
    public JsonResult<?> listPage(@RequestBody PagePO<ReleaseEntity> pagePO){
        log.info("===> 进入 /release/listPage ===> params = " + pagePO);
        IPage<ReleaseEntity> page = new PageQueryUtil<ReleaseEntity>().getPage(pagePO);
        LambdaQueryWrapper<ReleaseEntity> wrapper = new LambdaQueryWrapper<>();
        ReleaseEntity condition = pagePO.getCondition();
        if (!ObjectUtils.isEmpty(condition)) {
            wrapper.like(!ObjectUtils.isEmpty(condition.getReleaseName()), ReleaseEntity::getReleaseName, condition.getReleaseName());
        }
        wrapper.orderByDesc(ReleaseEntity::getAsRoof, ReleaseEntity::getReleaseSort, ReleaseEntity::getCreatedTime);
        PageVO<ReleaseEntity> result = releaseService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据id获取版本详情
     */
    @ApiOperation("根据id获取版本详情")
    @UserLoginToken(required = true, permission = { PermissionEnum.RELEASE_PAGE })
    @GetMapping("/release/getById")
    public JsonResult<?> getById(Integer id){
        // 暂时未用到
        return JsonResult.ok();
    }

    /**
     * 删除版本
     */
    @ApiOperation("删除版本")
    @UserLoginToken(required = true, permission = { PermissionEnum.RELEASE_DELETE })
    @PostMapping("/release/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO){
        if (ObjectUtils.isEmpty(idsPO)) {
            return JsonResult.fail("ids 不能为空！");
        }
        return releaseService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 设为版本标签置顶
     */
    @ApiOperation("设为版本标签置顶")
    @UserLoginToken(required = true, permission = { PermissionEnum.RELEASE_TOGGLE_ROOF })
    @PostMapping("/release/toggleRoof")
    public JsonResult<?> toggleRoof(Integer id, Boolean asRoof){
        if (ObjectUtils.isEmpty(id)) {
            return JsonResult.fail("id 不能为空！");
        }
        if (ObjectUtils.isEmpty(asRoof)) {
            return JsonResult.fail("asRoof 不能为空！");
        }
        return releaseService.toggleRoof(id, asRoof);
    }

    /**
     * 查询已设置为标签的版本列表
     */
    @ApiOperation("查询已设置为标签的版本列表")
    @GetMapping("/release/roofList")
    public JsonResult<?> roofList(){
        log.info("===> 进入 /release/roofList ===> ");
        LambdaQueryWrapper<ReleaseEntity> wrapper = new LambdaQueryWrapper<>();
        IPage<ReleaseEntity> page = new PageQueryUtil<ReleaseEntity>().getPage(new PagePO());
        wrapper.eq(ReleaseEntity::getAsRoof, true);
        wrapper.orderByDesc(ReleaseEntity::getReleaseSort, ReleaseEntity::getCreatedTime);
        PageVO<ReleaseEntity> result = releaseService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }
}
