package cn.workreport.modules.task.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务状态
 */
@AllArgsConstructor
public enum TaskStatusEnum implements ParamEnum {

    /**
     * 待审核
     */
    WAIT_AUDIT("1", "待审核"),

    /**
     * 审核完成
     */
    COMPLETE_AUDIT("2", "审核完成"),

    /**
     * 开发中
     */
    DEVELOPING("3", "开发中"),

    /**
     * 开发完成
     */
    COMPLETE_DEVELOP("4", "开发完成"),

    /**
     * 测试一轮
     */
    TEST_ONE("5", "测试一轮"),

    /**
     * 测试二轮
     */
    TEST_TWO("6", "测试二轮"),

    /**
     * 已关闭
     */
    CLOSED("7", "已关闭");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

//    @JsonCreator
//    public static TaskStatusEnum decode(final String value) {
//        return Stream.of(TaskStatusEnum.values()).filter(targetEnum -> targetEnum.value.equals(value)).findFirst().orElse(null);
//    }

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static TaskStatusEnum getEntityByVal (String value) {
        for (TaskStatusEnum entity : TaskStatusEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
