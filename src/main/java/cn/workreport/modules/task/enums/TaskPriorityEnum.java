package cn.workreport.modules.task.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务优先级
 */
@AllArgsConstructor
public enum TaskPriorityEnum implements ParamEnum {

    /**
     * 高
     */
    HIGH("1", "高"),

    /**
     * 中
     */
    MIDDLE("2", "中"),

    /**
     * 低
     */
    LOW("3", "低");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

//    @JsonCreator
//    public static TaskPriorityEnum decode(final String value) {
//        return Stream.of(TaskPriorityEnum.values()).filter(targetEnum -> targetEnum.value.equals(value)).findFirst().orElse(null);
//    }

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static TaskPriorityEnum getEntityByVal (String value) {
        for (TaskPriorityEnum entity : TaskPriorityEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
