package cn.workreport.modules.task.po;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class ReleaseConditionPO implements Serializable {
    public static final long serialVersionUID = 1L;

    private String releaseName;

}
