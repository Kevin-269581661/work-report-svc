package cn.workreport.modules.task.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class ReleasePagePO implements Serializable {
    public static final long serialVersionUID = 1L;

    private Map<String, Object> condition;

}
