package cn.workreport.modules.task.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * 接收客户端传来参数的对象
 */
@Data
public class ReleasePO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 版本名称
     */
    private String releaseName;

    /**
     * 版本描述
     */
    private String releaseDesc;

    /**
     * 版本排序
     */
    private Integer releaseSort;

}
