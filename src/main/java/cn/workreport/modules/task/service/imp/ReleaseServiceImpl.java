package cn.workreport.modules.task.service.imp;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.StrUtil;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.task.entity.ReleaseEntity;
import cn.workreport.modules.task.mapper.ReleaseMapper;
import cn.workreport.modules.task.service.IReleaseService;
import cn.workreport.util.JsonResult;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Slf4j
@Service
public class ReleaseServiceImpl extends ServiceImpl<ReleaseMapper, ReleaseEntity> implements IReleaseService {


    @Override
    public void wrapEntity(ReleaseEntity entity) {

    }

    @Override
    public PageVO<ReleaseEntity> pageEntity(IPage<ReleaseEntity> page, Wrapper<ReleaseEntity> queryWrapper) {
        IPage<ReleaseEntity> pageResult = this.page(page, queryWrapper);
        return new PageVO<>(pageResult);
    }

    @Override
    public ReleaseEntity getByIdEntity(Integer id) {
        return null;
    }

    @Override
    public JsonResult<?> saveOrUpdateEntity(ReleaseEntity entity) {
        if (StrUtil.isBlank(entity.getReleaseName())) {
            return JsonResult.fail("名称不能为空！");
        }

        ReleaseEntity releaseEntity = new ReleaseEntity();

        BeanUtil.copyProperties(
                entity,
                releaseEntity,
                CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true)
        );

        boolean isSuccess =  super.saveOrUpdate(releaseEntity);
        if (!isSuccess) {
            return JsonResult.fail("操作失败");
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> removeByIdEntity(Integer id) {
        boolean isSuccess = super.removeById(id);
        if (!isSuccess) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> removeByIdsEntity(List<Integer> ids) {
        for (Integer id : ids) {
            JsonResult<?> jsonResult = removeByIdEntity(id);
            if (!jsonResult.getState().equals(JsonResult.SUCCESS)) {
                return JsonResult.fail();
            }
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> toggleRoof(Integer id, Boolean asRoof) {
        ReleaseEntity releaseEntity = super.getById(id);
        if (ObjectUtils.isEmpty(releaseEntity)) {
            return JsonResult.fail("该条数据不存在");
        }
        // 最多可以开启设置5个
        if (asRoof == true) {
            LambdaQueryWrapper<ReleaseEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(ReleaseEntity::getAsRoof, true);
            long hasSettedCount = super.count(wrapper);
            log.info("hasSettedCount ===>" + hasSettedCount);
            if (hasSettedCount >= 5) {
                return JsonResult.fail("开启不能大于5条");
            }
        }
        releaseEntity.setAsRoof(asRoof);
        boolean isSuccess = super.saveOrUpdate(releaseEntity);
        if (!isSuccess) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }
}