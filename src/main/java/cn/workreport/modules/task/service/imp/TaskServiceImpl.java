package cn.workreport.modules.task.service.imp;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.workreport.modules.task.entity.TaskEntity;
import cn.workreport.modules.task.enums.TaskPriorityEnum;
import cn.workreport.modules.task.enums.TaskStatusEnum;
import cn.workreport.modules.task.mapper.TaskMapper;
import cn.workreport.modules.task.service.ITaskService;
import cn.workreport.util.EnumUtil;
import cn.workreport.util.JsonResult;
import cn.workreport.modules.common.vo.PageVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, TaskEntity> implements ITaskService {

    @Override
    public void wrapEntity(TaskEntity entity) {
        if (!ObjectUtils.isEmpty(entity.getTaskStatus())) {
            String taskStatusLabel = entity.getTaskStatus().getLabel();
            entity.setTaskStatusLabel(taskStatusLabel);
        }
        if (!ObjectUtils.isEmpty(entity.getTaskPriority())) {
            String taskPriorityLabel = entity.getTaskPriority().getLabel();
            entity.setTaskPriorityLabel(taskPriorityLabel);
        }
    }

    @Override
    public PageVO<TaskEntity> pageEntity(IPage<TaskEntity> page, Wrapper<TaskEntity> queryWrapper) {
        IPage<TaskEntity> pageResult = this.page(page, queryWrapper);
        return new PageVO<>(pageResult);
    }

    @Override
    public TaskEntity getByIdEntity(Integer id) {
        return null;
    }

    @Override
    public JsonResult<?> saveOrUpdateEntity(TaskEntity entity) {
        if (ObjectUtils.isEmpty(entity.getReleaseId())) {
            return JsonResult.fail("版本不能为空！");
        }
        if (ObjectUtils.isEmpty(entity.getTaskPriority())) {
            return JsonResult.fail("优先级不能为空！");
        }
        if (ObjectUtils.isEmpty(entity.getTaskTitle())) {
            return JsonResult.fail("任务标题不能为空！");
        }
        if (ObjectUtils.isEmpty(entity.getDemandUserId())) {
            return JsonResult.fail("需求负责人不能为空！");
        }

        TaskEntity taskEntity = new TaskEntity();

        BeanUtil.copyProperties(
                entity,
                taskEntity,
                CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true)
        );

        boolean isSuccess =  super.saveOrUpdate(taskEntity);
        if (!isSuccess) {
            return JsonResult.fail("操作失败");
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> removeByIdEntity(Integer id) {
        boolean isSuccess = super.removeById(id);
        if (!isSuccess) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> removeByIdsEntity(List<Integer> ids) {
        for (Integer id : ids) {
            JsonResult<?> jsonResult = removeByIdEntity(id);
            if (!jsonResult.getState().equals(JsonResult.SUCCESS)) {
                return JsonResult.fail();
            }
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> preparedData() {
        Map<String, Object> data = new HashMap<>();
        // 任务状态
        List<Map<String, Object>> taskStatusSelectList = EnumUtil.getSelectList(TaskStatusEnum.class);
        data.put("taskStatusSelectList", taskStatusSelectList);

        // 任务优先级
        List<Map<String, Object>> taskPrioritySelectList = EnumUtil.getSelectList(TaskPriorityEnum.class);
        data.put("taskPrioritySelectList", taskPrioritySelectList);

        return JsonResult.ok(data);
    }
}