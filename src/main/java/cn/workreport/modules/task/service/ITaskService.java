package cn.workreport.modules.task.service;


import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.task.entity.TaskEntity;
import cn.workreport.util.JsonResult;

/**
 * 任务模块接口
 */
public interface ITaskService extends BaseService<TaskEntity> {


    /**
     * 获取预加载数据
     * @return
     */
    JsonResult preparedData();
}

