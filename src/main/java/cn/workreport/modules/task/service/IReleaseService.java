package cn.workreport.modules.task.service;


import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.task.entity.ReleaseEntity;
import cn.workreport.util.JsonResult;

/**
 * 版本模块接口
 */
public interface IReleaseService extends BaseService<ReleaseEntity> {

    /**
     * 设为标签置顶
     * @param id
     * @param asRoof
     * @return
     */
    JsonResult<?> toggleRoof (Integer id, Boolean asRoof);
}

