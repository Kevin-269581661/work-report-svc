package cn.workreport.modules.task.entity;

import cn.workreport.modules.common.annotations.MyToDoing;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ReleaseEntity", description = "版本")
@TableName("t_release")
public class ReleaseEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 版本名称
     */
    @MyToDoing(doing = true, rename = true, renameValue = "aaa")
    @ApiModelProperty("版本名称")
    @TableField
    private String releaseName;

    /**
     * 版本描述
     */
    @ApiModelProperty("版本描述")
    @TableField
    private String releaseDesc;

    /**
     * 版本排序
     */
    @ApiModelProperty("版本排序")
    @TableField
    private Integer releaseSort;

    /**
     * 是否设为标签置顶显示
     */
    @MyToDoing
    @ApiModelProperty("是否设为标签置顶显示")
    @TableField("is_roof")
    private Boolean asRoof;

}
