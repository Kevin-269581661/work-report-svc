package cn.workreport.modules.task.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import cn.workreport.modules.task.enums.TaskPriorityEnum;
import cn.workreport.modules.task.enums.TaskStatusEnum;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TaskEntity", description = "任务")
@TableName("t_task")
public class TaskEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模块ids，使用|连接的字符串
     */
    @ApiModelProperty("模块ids，使用|连接的字符串")
    @TableField
    private String moduleIds;

    /**
     * 模块名称，使用|连接的字符串
     */
    @ApiModelProperty("模块名称，使用|连接的字符串")
    @TableField
    private String moduleNames;

    /**
     * 任务说明
     */
    @ApiModelProperty("任务说明")
    @TableField
    private String taskDesc;

    /**
     * 任务标题
     */
    @ApiModelProperty("任务标题")
    @TableField
    private String taskTitle;

    /**
     * 任务状态
     */
    @ApiModelProperty("任务状态")
    @TableField
    private TaskStatusEnum taskStatus;

    /**
     * 任务备注
     */
    @ApiModelProperty("任务备注")
    @TableField
    private String taskRemark;

    /**
     * 任务优先级
     */
    @ApiModelProperty("任务优先级")
    @TableField
    private TaskPriorityEnum taskPriority;

    /**
     * 任务难度
     */
    @ApiModelProperty("任务难度")
    @TableField
    private String taskDifficulty;

    /**
     * 需求负责人id
     */
    @ApiModelProperty("需求负责人id")
    @TableField
    private Integer demandUserId;

    /**
     * 需求完成时间
     */
    @ApiModelProperty("需求完成时间")
    @TableField
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date demandCompleteTime;

    /**
     * 需求状态
     */
    @ApiModelProperty("需求状态")
    @TableField
    private String demandStatus;

    /**
     * 需求评审时间
     */
    @ApiModelProperty("需求评审时间")
    @TableField
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date demandAuditTime;

    /**
     * 开发负责人
     */
    @ApiModelProperty("开发负责人")
    @TableField
    private Integer workUserId;

    /**
     * 开发计划完成时间
     */
    @ApiModelProperty("开发计划完成时间")
    @TableField
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date workPlanCompleteTime;

    /**
     * 开发完成程度
     */
    @ApiModelProperty("开发完成程度")
    @TableField
    private Integer workProgress;

    /**
     * 开发预估工作量（天）
     */
    @ApiModelProperty("开发预估工作量（天）")
    @TableField
    private Integer workPlanDays;

    /**
     * 开发备注
     */
    @ApiModelProperty("开发备注")
    @TableField
    private String workRemark;

    /**
     * 开发实际完成时间
     */
    @ApiModelProperty("开发实际完成时间")
    @TableField
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date workActualCompleteTime;

    /**
     * 开发实际投入时间（天）
     */
    @ApiModelProperty("开发实际投入时间（天）")
    @TableField
    private Integer workActualDays;

    /**
     * 是否需要测试
     */
    @ApiModelProperty("是否需要测试")
    @TableField("is_need_test")
    private Boolean asNeedTest;

    /**
     * 是否通过冒烟测试
     */
    @ApiModelProperty("是否通过冒烟测试")
    @TableField("is_pass_dev_test")
    private Boolean asPassDevTest;

    /**
     * 测试说明
     */
    @ApiModelProperty("测试说明")
    @TableField
    private String testDesc;

    /**
     * 测试负责人
     */
    @ApiModelProperty("测试负责人")
    @TableField
    private Integer testUserId;

    /**
     * 测试开始时间
     */
    @ApiModelProperty("测试开始时间")
    @TableField
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date testStartTime;

    /**
     * 测试一轮进度
     */
    @ApiModelProperty("测试一轮进度")
    @TableField
    private Integer testOneProgress;

    /**
     * 测试二轮进度
     */
    @ApiModelProperty("测试二轮进度")
    @TableField
    private Integer testTwoProgress;

    /**
     * 测试回归进度
     */
    @ApiModelProperty("测试回归进度")
    @TableField
    private Integer testBackProgress;

    /**
     * 测试备注
     */
    @ApiModelProperty("测试备注")
    @TableField
    private String testRemark;

    /**
     * 版本id
     */
    @ApiModelProperty("版本id")
    @TableField
    private Integer releaseId;


    /*    ----------------------- 以下为非表字段 ---------------------------     */
    @TableField(exist = false)
    private String taskStatusLabel;

    @TableField(exist = false)
    private String taskPriorityLabel;

}
