package cn.workreport.modules.report.vo;

import cn.workreport.modules.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 日报提交记录 发送前端数据
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ReportCommitVO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 上个工作日完成的日报记录
     */
    private List<?> lastFinishReportList;

    /**
     * 今日计划工作的日报记录
     */
    private List<?> todayPlanReportList;

    /**
     * 备注
     */
    private String reportCommitRemark;

//--------------------------------------以下为非表字段--------------------------------------------------

//--------------------------------------以上为非表字段---------------------------------------------
}
