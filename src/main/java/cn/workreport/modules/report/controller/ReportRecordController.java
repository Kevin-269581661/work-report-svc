package cn.workreport.modules.report.controller;

import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;

import cn.workreport.modules.report.entity.ReportRecord;
import cn.workreport.modules.report.service.IReportRecordService;

/**
 * <p>
 * 日报提交记录的内容 对应控制器
 * </p>
 *
 * @author yyf
 * @since 2022-04-03
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "日报提交记录的内容模块")
public class ReportRecordController extends BaseController {

    @Autowired
    private IReportRecordService reportRecordService;

    /**
     * 保存或修改日报提交记录的内容
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存日报提交记录的内容")
    @PostMapping("/reportRecord/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(ReportRecord entity) {
        log.info("===> 进入 /reportRecord/saveOrUpdate ===> params: entity=" + entity);
        return reportRecordService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询日报提交记录的内容
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询日报提交记录的内容")
    @PostMapping("/reportRecord/listPage")
    public JsonResult<?> listPage(PagePO<ReportRecord> pagePO){
        log.info("===> 进入 /reportRecord/listPage ===> params: pagePO=" + pagePO);
        IPage<ReportRecord> page = new PageQueryUtil<ReportRecord>().getPage(pagePO);
        LambdaQueryWrapper<ReportRecord> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(ReportRecord::getCreatedTime);
        PageVO<ReportRecord> result = reportRecordService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询日报提交记录的内容
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询日报提交记录的内容")
    @GetMapping("/reportRecord/getById")
    public JsonResult<?> getById(Integer id){
        log.info("===> 进入 /reportRecord/getById ===> params: id=" + id);
        if (ObjectUtils.isEmpty(id)) {
            return JsonResult.fail("参数 id 不能为空！");
        }
        ReportRecord entity = reportRecordService.getByIdEntity(id);
        return JsonResult.ok(entity);
    }

    /**
    * 批量或单体删除日报提交记录的内容数据
    *
    * @param idsPO ID集合
    * @return 成功与否
    */
    @ApiOperation("批量删除日报提交记录的内容")
    @PostMapping("/reportRecord/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO){
        log.info("===> 进入 /reportRecord/removeByIds ===> params: idsPO=" + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("参数 ids 不能为空！");
        }
        return reportRecordService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 日报提交记录的内容预加载数据
     *
     * @return 预加载数据列表的 Map
     */
    // @ApiOperation("日报提交记录的内容预加载数据")
    // @GetMapping("/reportRecord/preparedData")
    // public JsonResult<?> preparedData() {
        //   log.info("===> 进入 /reportRecord/preparedData ===>");
        //   return reportRecordService.preparedData();
    // }

}
