package cn.workreport.modules.report.controller;

import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.report.entity.ReportEntity;
import cn.workreport.modules.report.po.ReportPO;
import cn.workreport.modules.report.service.IReportService;
import cn.workreport.modules.users.entity.UserEntity;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import cn.workreport.util.UserChacheFromToken;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;


/**
 * 日报模块 controller
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "日报模块")
public class ReportController extends BaseController {
    @Autowired
    private IReportService reportService;

    /**
     * 分页查询当前登录用户日报列表
     */
    @ApiOperation("分页查询当前登录用户日报列表")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_PAGE})
    @PostMapping("/report/listPage")
    public JsonResult<?> listPage(@RequestBody PagePO<ReportEntity> pagePO) {
        log.info("===> 进入 /report/listPage ===> params = " + pagePO);
        // 取出当前登录用户信息
        UserEntity currentLoginUser = UserChacheFromToken.getUser();
        // 执行查询
        IPage<ReportEntity> page = new PageQueryUtil<ReportEntity>().getPage(pagePO);
        LambdaQueryWrapper<ReportEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ReportEntity::getCreatorId, currentLoginUser.getId());
        ReportEntity condition = pagePO.getCondition();
        if (!ObjectUtils.isEmpty(condition)) {
            wrapper.ge(!ObjectUtils.isEmpty(condition.getStartDateTime()), ReportEntity::getReportTime, condition.getStartDateTime());
            wrapper.le(!ObjectUtils.isEmpty(condition.getEndDateTime()), ReportEntity::getReportTime, condition.getEndDateTime());
        }
        wrapper.orderByDesc(ReportEntity::getReportTime);
        PageVO<ReportEntity> result = reportService.pageEntity(page, wrapper);
        if (!ObjectUtils.isEmpty(result.getList())) {
            result.getList().forEach(taskEntity -> reportService.wrapEntity(taskEntity));
        }
        return JsonResult.ok(result);
    }

    /**
     * 查询当前登录的用户的日报列表
     */
    @ApiOperation("查询当前登录的用户的日报列表")
    @GetMapping("/report/list")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_PAGE})
    public JsonResult<?> list(PagePO<ReportPO> params) {
        log.info("======== 进入 /workreport/list ======== params = " + params);
        PageVO<ReportEntity> page = reportService.queryPage(params);
        return JsonResult.ok(page);
    }

    /**
     * 查询日报信息
     */
    @ApiOperation("查询日报信息")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_PAGE})
    @GetMapping("/report/info/{id}")
    public JsonResult<?> info(@PathVariable("id") Integer id) {
        log.info("======== 进入 /workreport/info ========>  id = " + id);
        ReportEntity report = reportService.getById(id);
        return JsonResult.ok(report);
    }

    /**
     * 保存日报
     */
    @ApiOperation("保存日报")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_SAVE_OR_UPDATE})
    @PostMapping("/report/save")
    public JsonResult<?> save(ReportPO report) {
        log.info("======== 进入 /workreport/save ========> report = " + report);
        return reportService.saveReport(report);
    }

    /**
     * 修改日报
     */
    @ApiOperation("修改日报")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_SAVE_OR_UPDATE})
    @PostMapping("/report/update")
    public JsonResult<?> update(ReportEntity report) {
        log.info("======== 进入 /workreport/update ========> report" + report);
        reportService.updateById(report);
        return JsonResult.ok();
    }

    /**
     * 删除日报
     */
    @ApiOperation("删除日报")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_DELETE})
    @PostMapping("/report/delete")
    public JsonResult delete(IdsPO idsPO) {
        log.info("======== 进入 /workreport/delete ========> idsPO = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("ids 不能为空！");
        }
        reportService.removeByIds(idsPO.getIds());
        return JsonResult.ok();
    }

    /**
     * 生成并导出周报
     */
    @ApiOperation("生成并导出周报")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_EXPORT})
    @PostMapping("/report/exportWeekReport")
    public void exportWeekReport(@RequestBody ReportPO params) {
        reportService.exportWeekReport(params);
    }

    /**
     * 统计一周日报数量和一年日报数量
     *
     * @return
     */
    @ApiOperation("统计一周日报数量和一年日报数量")
    @GetMapping("/report/statistics")
    public JsonResult<?> statistics() {
        return reportService.statistics();
    }

    /**
     * 获取登录用户的上个工作日的日报列表
     *
     * @return
     */
    @ApiOperation("获取登录用户的上个工作日的日报列表")
    @GetMapping("/report/lastWorkDayReportSelf")
    public JsonResult<?> lastWorkDayReportSelf() {
        return reportService.lastWorkDayReportSelf();
    }

}
