package cn.workreport.modules.report.controller;

import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.report.entity.ReportCommit;
import cn.workreport.modules.report.po.ReportCommitPO;
import cn.workreport.modules.report.service.IReportCommitService;
import cn.workreport.modules.users.entity.UserEntity;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import cn.workreport.util.UserChacheFromToken;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 日报提交记录 前端控制器
 * </p>
 *
 * @author yyf
 * @since 2022-02-25
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "日报发送模块")
public class ReportCommitController extends BaseController {

    @Autowired
    private IReportCommitService reportCommitService;

    /**
     * 保存发送的日报记录
     *
     * @param reportCommitPO 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("保存发送的日报记录")
    @UserLoginToken(permission = {PermissionEnum.REPORT_SEND})
    @PostMapping("/reportCommit/sendTodayReport")
    public JsonResult<?> sendTodayReport(ReportCommitPO reportCommitPO) {
        log.info("===> 进入 /reportCommit/sendTodayReport ===> params = " + reportCommitPO);
        return reportCommitService.sendTodayReport(
                reportCommitPO.getLastFinishReportIds(),
                reportCommitPO.getTodayPlanReportIds(),
                reportCommitPO.getReportCommitRemark()
        );
    }

    /**
     * 保存或修改
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("保存或修改")
    @UserLoginToken(permission = {PermissionEnum.REPORT_SEND})
    @PostMapping("/reportCommit/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(@RequestBody ReportCommit entity) {
        log.info("===> 进入 /reportCommit/saveOrUpdate ===> params: entity=" + entity);
        return reportCommitService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询所有发送的日报记录
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询所有发送的日报记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_COMMIT_PAGE})
    @PostMapping("/reportCommit/listPage")
    public JsonResult<?> listPage(PagePO<ReportCommit> pagePO) {
        log.info("===> 进入 /reportCommit/listPage ===> params = " + pagePO);
        IPage<ReportCommit> page = new PageQueryUtil<ReportCommit>().getPage(pagePO);
        LambdaQueryWrapper<ReportCommit> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(ReportCommit::getCreatedTime);
        PageVO<ReportCommit> result = reportCommitService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询日报提交记录
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询日报提交记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_COMMIT_PAGE})
    @GetMapping("/reportCommit/getById")
    public JsonResult<?> getById(Integer id) {
        log.info("===> 进入 /reportCommit/getById ===> params = " + id);
        // 暂时未用到
        return JsonResult.ok();
    }

    /**
     * 批量或单体删除日报提交记录
     *
     * @param idsPO ID集合
     * @return 成功与否
     */
    @ApiOperation("批量删除日报提交记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_COMMIT_DELETE})
    @PostMapping("/reportCommit/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO) {
        log.info("===> 进入 /reportCommit/removeByIds ===> params = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO)) {
            return JsonResult.fail("ids 不能为空！");
        }
        return reportCommitService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 查询当前登录用户的今日日报提交记录
     *
     * @return
     */
    @ApiOperation("查询当前登录用户的今日日报提交记录")
    @GetMapping("/reportCommit/getCommitCountSelf")
    public JsonResult<?> getCommitCountSelf() {
        log.info("===> 进入 /reportCommit/getCommitCountSelf ===>");
        return reportCommitService.getCommitCountSelf();
    }

    /**
     * 查询自己的日报提交记录
     *
     * @return 查询结果
     */
    @ApiOperation("查询自己的日报提交记录")
    @UserLoginToken(required = true, permission = {PermissionEnum.REPORT_SEND})
    @PostMapping("/reportCommit/listPageSelf")
    public JsonResult<?> listPageSelf(@RequestBody PagePO<ReportCommit> pagePO) {
        log.info("===> 进入 /reportCommit/listPageSelf ===> params = " + pagePO);
        // 取出当前登录用户信息
        UserEntity currentLoginUser = UserChacheFromToken.getUser();
        // 执行查询
        IPage<ReportCommit> page = new PageQueryUtil<ReportCommit>().getPage(pagePO);
        LambdaQueryWrapper<ReportCommit> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ReportCommit::getCreatorId, currentLoginUser.getId());
        ReportCommit condition = pagePO.getCondition();
        if (!ObjectUtils.isEmpty(condition)) {
            wrapper.ge(!ObjectUtils.isEmpty(condition.getStartCreatedTime()), ReportCommit::getCreatedTime, condition.getStartCreatedTime());
            wrapper.le(!ObjectUtils.isEmpty(condition.getEndCreatedTime()), ReportCommit::getCreatedTime, condition.getEndCreatedTime());
        }
        wrapper.orderByDesc(ReportCommit::getCreatedTime);
        PageVO<ReportCommit> result = reportCommitService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

}
