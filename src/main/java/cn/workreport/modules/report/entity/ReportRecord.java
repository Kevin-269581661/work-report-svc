package cn.workreport.modules.report.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 日报提交记录的内容对应实体类
 * </p>
 *
 * @author yyf
 * @since 2022-04-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_report_record")
@ApiModel(value = "ReportRecord", description = "日报提交记录的内容对应实体类")
public class ReportRecord extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("模块ids，使用|连接的字符串")
    @TableField
    private String moduleIds;

    @ApiModelProperty("模块名称，使用|连接的字符串")
    @TableField
    private String moduleNames;

    @ApiModelProperty("分隔符")
    @TableField
    private String splitChar;

    @ApiModelProperty("报告内容")
    @TableField
    private String reportText;

    @ApiModelProperty("报告人昵称")
    @TableField
    private String reportName;

    @ApiModelProperty("报告的时间")
    @TableField
    private LocalDateTime reportTime;

    @ApiModelProperty("日报提交记录的id")
    @TableField
    private Integer reportCommitId;

    @ApiModelProperty("工作进度")
    @TableField
    private Integer workProgress;

    @ApiModelProperty("机构id")
    @TableField
    private Integer orgId;

// --------------------------------------以下为非表字段-------------------------------------------- //

// --------------------------------------以上为非表字段-------------------------------------------- //
}
