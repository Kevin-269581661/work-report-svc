package cn.workreport.modules.report.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * 
 * @author yyf
 * @email sunlightcs@gmail.com
 * @date 2021-11-10 13:04:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Report对象", description = "日报")
@TableName("t_report")
public class ReportEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 项目id
	 */
	@ApiModelProperty("项目id")
	@TableField
	private String moduleIds;

	/**
	 * 模块id
	 */
	@ApiModelProperty("模块id")
	@TableField
	private String moduleNames;

	/**
	 * 分隔符
	 */
	@ApiModelProperty("分隔符")
	@TableField
	private String splitChar;

	/**
	 * 报告内容
	 */
	@ApiModelProperty("报告内容")
	@TableField
	private String reportText;

	/**
	 * 报告人名
	 */
	@ApiModelProperty("报告人名")
	@TableField
	private String reportName;

	/**
	 * 报告时间
	 */
	@ApiModelProperty("报告时间")
	@TableField
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat( pattern = "yyyy-MM-dd")
	private Date reportTime;

	/**
	 * 工作进度
	 */
	@ApiModelProperty("工作进度")
	@TableField
	private Integer workProgress;

	/* ------------------------- 以下为非表字段 ------------------------------------ */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(exist = false)
	private Date startDateTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@TableField(exist = false)
	private Date endDateTime;

}
