package cn.workreport.modules.report.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 日报提交记录
 * </p>
 *
 * @author yyf
 * @since 2022-02-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "t_report_commit", autoResultMap = true)
@ApiModel(value = "ReportCommit对象", description = "日报提交记录")
public class ReportCommit extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("上个工作日完成的日报记录（json字符串格式）")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private  List<ReportEntity> lastFinishReportList;

    @ApiModelProperty("今日计划工作的日报记录（json字符串格式）")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<ReportEntity> todayPlanReportList;

    @ApiModelProperty("备注")
    @TableField
    private String reportCommitRemark;

//--------------------------------------以下为非表字段--------------------------------------------------
    /**
     * 日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(exist = false)
    private Date startCreatedTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(exist = false)
    private Date endCreatedTime;
//--------------------------------------以上为非表字段---------------------------------------------
}
