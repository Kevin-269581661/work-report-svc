package cn.workreport.modules.report.mapper;

import cn.workreport.modules.report.entity.ReportCommit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 日报提交记录 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-25
 */
@Mapper
public interface ReportCommitMapper extends BaseMapper<ReportCommit> {

}
