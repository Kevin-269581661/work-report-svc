package cn.workreport.modules.report.mapper;

import cn.workreport.modules.report.entity.ReportRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 日报提交记录的内容 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-04-03
 */
@Mapper
public interface ReportRecordMapper extends BaseMapper<ReportRecord> {

}
