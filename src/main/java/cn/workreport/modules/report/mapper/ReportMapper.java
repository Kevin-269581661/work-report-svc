package cn.workreport.modules.report.mapper;

import cn.workreport.modules.report.entity.ReportEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 
 * 
 * @author yyf
 * @email sunlightcs@gmail.com
 * @date 2021-11-10 13:04:34
 */
@Mapper
public interface ReportMapper extends BaseMapper<ReportEntity> {

//    IPage<ReportEntity> findLoginUserReportListByDate(
//            Page<?> page,
//            @Param("creatorId") Integer creatorId,
//            @Param("createdTime") Date createdTime
//    );
}
