package cn.workreport.modules.report.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class NextWeekPlanItem implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 下周工作项目
     */
    private String projectName;

    /**
     * 计划达成目标
     */
    private String planTarget;

}
