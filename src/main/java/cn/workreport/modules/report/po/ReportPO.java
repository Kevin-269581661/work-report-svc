package cn.workreport.modules.report.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 接收参数实体
 * @author yyf
 * @email sunlightcs@gmail.com
 * @date 2021-11-10 13:04:34
 */
@Data
public class ReportPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;

	/**
	 * 模块id (使用 | 拼接成的 String)
	 */
	private String moduleIds;

	/**
	 * 模块名称 (使用 | 拼接成的 String)
	 */
	private String moduleNames;

	/**
	 * 分隔符
	 */
	private String splitChar;

	/**
	 * 报告内容
	 */
	private String reportText;

	/**
	 * 报告人姓名 （使用的是昵称 nickname）
	 */
	private String reportName;

	/**
	 * 工作进度
	 */
	private String workProgress;

	/**
	 * 报告时间（接收的是 String）
	 */
	private String reportTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date startDateTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date endDateTime;

	/**
	 *  周報id列表
	 */
	private List<String> reportIds;

	private List<NextWeekPlanItem> nextWeekPlanList;

}
