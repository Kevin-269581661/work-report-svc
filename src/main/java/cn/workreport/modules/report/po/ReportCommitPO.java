package cn.workreport.modules.report.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class ReportCommitPO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 上个工作日完成的日报记录 ids
     */
    private List<Integer> lastFinishReportIds;

    /**
     * 今日计划工作的日报记录 ids
     */
    private List<Integer> todayPlanReportIds;

    /**
     * 备注
     */
    private String reportCommitRemark;

    /**
     * 日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startCreatedTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endCreatedTime;

    /**
     * 机构id
     */
    private Integer orgId;
}
