package cn.workreport.modules.report.service.impl;

import cn.workreport.modules.report.entity.ReportRecord;
import cn.workreport.modules.report.mapper.ReportRecordMapper;
import cn.workreport.modules.report.service.IReportRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.util.JsonResult;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 * 日报提交记录的内容 业务接口实现类
 * </p>
 *
 * @author yyf
 * @since 2022-04-03
 */
@Slf4j
@Service
public class ReportRecordServiceImpl extends ServiceImpl<ReportRecordMapper, ReportRecord> implements IReportRecordService {

    @Override
    public void wrapEntity(ReportRecord entity) {
        if (ObjectUtils.isEmpty(entity)) {
            return;
        }
    }

    @Override
    public PageVO<ReportRecord> pageEntity(IPage<ReportRecord> page, Wrapper<ReportRecord> queryWrapper) {
        IPage<ReportRecord> pageResult = this.page(page, queryWrapper);
        if (!ObjectUtils.isEmpty(pageResult.getRecords())) {
            pageResult.getRecords().forEach(entity -> {
                this.wrapEntity(entity);
            });
        }
        return new PageVO<>(pageResult);
    }

    @Override
    public ReportRecord getByIdEntity(Integer id) {
        ReportRecord entity = this.getById(id);
        this.wrapEntity(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> saveOrUpdateEntity(ReportRecord entity) {
        if (!this.saveOrUpdate(entity)) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdEntity(Integer id) {
        if (!this.removeById(id)) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdsEntity(List<Integer> ids) {
        for (Integer id : ids) {
            JsonResult<?> jsonResult = removeByIdEntity(id);
            if (!JsonResult.SUCCESS.equals(jsonResult.getState())) {
                return JsonResult.fail();
            }
        }
        return JsonResult.ok();
    }

    // @Override
    // public JsonResult<?> preparedData() {
    //     Map<String, Object> data = new HashMap<>();
    //     // 示例
    //     List<Map<String, Object>> deletedSelectList = EnumUtil.getSelectList(IsEnum.class);
    //     data.put("deletedSelectList", deletedSelectList);
    //     return JsonResult.ok(data);
    // }

}

