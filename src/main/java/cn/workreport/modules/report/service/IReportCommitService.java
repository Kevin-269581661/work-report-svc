package cn.workreport.modules.report.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.report.entity.ReportCommit;
import cn.workreport.util.JsonResult;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 日报提交记录 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-25
 */
public interface IReportCommitService extends BaseService<ReportCommit> {

    /**
     * 保存日报提交记录
     *
     * @param lastFinishReportIds 上个工作日完成的日报记录 ids
     * @param todayPlanReportIds  今日计划工作的日报记录 ids
     * @param reportCommitRemark  备注
     * @return JsonResult
     */
    JsonResult<?> sendTodayReport(List<Integer> lastFinishReportIds, List<Integer> todayPlanReportIds, String reportCommitRemark);

    /**
     * 查询当前登录用户的今日日报提交记录
     *
     * @return JsonResult
     */
    JsonResult<?> getCommitCountSelf();

}

