package cn.workreport.modules.report.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.report.entity.ReportRecord;

/**
 * <p>
 * 日报提交记录的内容 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-04-03
 */
public interface IReportRecordService extends BaseService<ReportRecord> {
    /**
     * 日报提交记录的内容预加载数据
     *
     * @return 预加载数据列表的 Map
     */
    // JsonResult<?> preparedData();
}

