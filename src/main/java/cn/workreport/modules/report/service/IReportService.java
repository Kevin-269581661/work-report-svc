package cn.workreport.modules.report.service;


import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.report.entity.ReportEntity;
import cn.workreport.modules.report.po.ReportPO;
import cn.workreport.util.JsonResult;

/**
 * @author yyf
 * @email sunlightcs@gmail.com
 * @date 2021-11-10 13:04:34
 */
public interface IReportService extends BaseService<ReportEntity> {

    /**
     * 查询当前登录的用户的日报列表
     *
     * @param params
     * @return
     */
    PageVO<ReportEntity> queryPage(PagePO<ReportPO> params);

    JsonResult<?> saveReport(ReportPO report);

    /**
     * 导出周报
     *
     * @param param
     * @return
     */
    void exportWeekReport(ReportPO param);

    /**
     * 统计一周数量和一年数量
     *
     * @return
     */
    JsonResult<?> statistics();

    /**
     * 获取登录用户的上个工作日的日报列表
     *
     * @return
     */
    JsonResult<?> lastWorkDayReportSelf();
}

