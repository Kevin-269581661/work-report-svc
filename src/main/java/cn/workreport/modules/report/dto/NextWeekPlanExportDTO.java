package cn.workreport.modules.report.dto;


import cn.workreport.modules.common.annotations.ExcelTitle;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@ContentFontStyle(fontHeightInPoints = 16)
@ContentRowHeight(90)
@EqualsAndHashCode
@Accessors(chain = true)
public class NextWeekPlanExportDTO {

    @ExcelTitle("下周工作项目")
    private String projectName;

    @ExcelTitle("计划达成目标")
    private String planTarget;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPlanTarget() {
        return planTarget;
    }

    public void setPlanTarget(String planTarget) {
        this.planTarget = planTarget;
    }

}
