package cn.workreport.modules.report.dto;

import cn.workreport.modules.common.annotations.ExcelTitle;
import com.alibaba.excel.annotation.write.style.ContentFontStyle;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 描述【周报导出实体】
 *
 * @author lihuanyao
 * @time 2021/11/26 18:26
 */
// 内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 16)
@ContentRowHeight(90)
@EqualsAndHashCode
@Accessors(chain = true)
public class WeekExportDTO {
    @ExcelTitle("工作项目")
    private String workModule;

    @ExcelTitle("原计划完成目标")
    private String originalTarget;

    @ExcelTitle("实际完成情况及数据说明")
    private String actualTarget;

    @ExcelTitle("下周工作项目")
    private String projectName;

    @ExcelTitle("计划达成目标")
    private String planTarget;

    /**
     *  工作项目集合
     */
    private Set<String> workModuleSet = new HashSet<>();
    /**
     *  原计划项目集合
     */
    private List<String> originalTargetSet = new ArrayList<>();
    /**
     *  实际完成情况数据说明
     */
    private List<String> actualTargetSet = new ArrayList<>();
    /**
     *  序号
     */
    private Integer orderNum = 0;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPlanTarget() {
        return planTarget;
    }

    public void setPlanTarget(String planTarget) {
        this.planTarget = planTarget;
    }

    public String getWorkModule() {
        return workModule;
    }

    public void setWorkModule(String workModule) {
        this.workModule = workModule;
    }

    public String getOriginalTarget() {
        return originalTarget;
    }

    public void setOriginalTarget(String originalTarget) {
        this.originalTarget = originalTarget;
    }

    public String getActualTarget() {
        return actualTarget;
    }

    public void setActualTarget(String actualTarget) {
        this.actualTarget = actualTarget;
    }

    public Set<String> getWorkModuleSet() {
        return workModuleSet;
    }

    public void setWorkModuleSet(Set<String> workModuleSet) {
        this.workModuleSet = workModuleSet;
    }

    public List<String> getOriginalTargetSet() {
        return originalTargetSet;
    }

    public void setOriginalTargetSet(List<String> originalTargetSet) {
        this.originalTargetSet = originalTargetSet;
    }

    public List<String> getActualTargetSet() {
        return actualTargetSet;
    }

    public void setActualTargetSet(List<String> actualTargetSet) {
        this.actualTargetSet = actualTargetSet;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
}
