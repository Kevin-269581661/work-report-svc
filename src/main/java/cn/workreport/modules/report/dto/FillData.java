package cn.workreport.modules.report.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述【】
 *
 * @author lihuanyao
 * @time 2021/11/27 2:03
 */
@Data
@EqualsAndHashCode
public class FillData {
    private String name;
    private String department;
    private String date;
}
