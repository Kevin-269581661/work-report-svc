package cn.workreport.modules.report.dto;


import cn.workreport.modules.report.entity.ReportCommit;
import cn.workreport.modules.users.enums.SexEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserReportCommitDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String username; // "用户名"

    private String nickname; // "昵称"

    private SexEnum gender;// "性别：0-女，1-男"

    private String avatar; // "头像"

    private String truename; // 姓名

    private Boolean isLeader; // 是否是组织的组长

    private List<ReportCommit> reportCommitList;

}
