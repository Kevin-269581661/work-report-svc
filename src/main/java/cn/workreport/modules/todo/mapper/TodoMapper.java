package cn.workreport.modules.todo.mapper;

import cn.workreport.modules.todo.entity.Todo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 待办 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-05-11
 */
@Mapper
public interface TodoMapper extends BaseMapper<Todo> {

}
