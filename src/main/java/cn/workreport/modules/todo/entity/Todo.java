package cn.workreport.modules.todo.entity;

import cn.workreport.modules.common.enums.IsEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 待办对应实体类
 * </p>
 *
 * @author yyf
 * @since 2022-05-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_todo")
@ApiModel(value = "Todo", description = "待办对应实体类")
public class Todo extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("待办标题")
    @TableField
    private String title;

    @ApiModelProperty("待办内容")
    @TableField
    private String content;

    @ApiModelProperty("待办人id")
    @TableField
    private Integer todoUserId;

    @ApiModelProperty("是否完成待办")
    @TableField
    private IsEnum isFinish;

    @ApiModelProperty("待办类型")
    @TableField
    private String type;

    @ApiModelProperty("待办时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @TableField
    private Date todoTime;

// --------------------------------------以下为非表字段-------------------------------------------- //
    @TableField(exist = false)
    private String isFinishLabel;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(exist = false)
    private Date startDateTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(exist = false)
    private Date endDateTime;
// --------------------------------------以上为非表字段-------------------------------------------- //
}
