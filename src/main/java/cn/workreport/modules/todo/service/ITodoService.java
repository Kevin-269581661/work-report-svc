package cn.workreport.modules.todo.service;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.todo.entity.Todo;
import cn.workreport.util.JsonResult;

import java.util.List;

/**
 * <p>
 * 待办 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-05-11
 */
public interface ITodoService extends BaseService<Todo> {
    /**
     * 待办预加载数据
     *
     * @return 预加载数据列表的 Map
     */
    JsonResult<?> preparedData();

    /**
     * 设置待办是否完成
     *
     * @param id       待办id
     * @param isFinish 是否完成
     * @return JsonResult
     */
    JsonResult<?> toggleIsFinish(Integer id, IsEnum isFinish);

    /**
     * 查询今日待办
     *
     * @return
     */
    List<Todo> listTodayWaitDo();
}

