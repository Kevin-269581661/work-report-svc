package cn.workreport.modules.todo.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.exception.ServiceException;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.todo.entity.Todo;
import cn.workreport.modules.todo.mapper.TodoMapper;
import cn.workreport.modules.todo.service.ITodoService;
import cn.workreport.util.EnumUtil;
import cn.workreport.util.JsonResult;
import cn.workreport.util.UserChacheFromToken;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 待办 业务接口实现类
 * </p>
 *
 * @author yyf
 * @since 2022-05-11
 */
@Slf4j
@Service
public class TodoServiceImpl extends ServiceImpl<TodoMapper, Todo> implements ITodoService {

    @Override
    public void wrapEntity(Todo entity) {
        if (ObjectUtils.isEmpty(entity)) {
            return;
        }
        if (ObjectUtil.isNotEmpty(entity.getIsFinish())) {
            String isFinishLabel = entity.getIsFinish().getLabel();
            entity.setIsFinishLabel(isFinishLabel);
        }
    }

    @Override
    public PageVO<Todo> pageEntity(IPage<Todo> page, Wrapper<Todo> queryWrapper) {
        IPage<Todo> pageResult = this.page(page, queryWrapper);
        List<Todo> entityList = pageResult.getRecords();
        if (!ObjectUtils.isEmpty(entityList)) {
            entityList.forEach(entity -> {
                this.wrapEntity(entity);
            });
        }
        return new PageVO<>(pageResult);
    }

    @Override
    public Todo getByIdEntity(Integer id) {
        Todo entity = this.getById(id);
        this.wrapEntity(entity);
        return entity;
    }

    @Override
    public JsonResult<?> saveOrUpdateEntity(Todo entity) {
        if (ObjectUtil.isEmpty(entity.getTitle())) {
            return JsonResult.fail("参数 title 不能为空");
        }
        if (ObjectUtil.isEmpty(entity.getTodoUserId())) {
            return JsonResult.fail("参数 todoUserId 不能为空");
        }
        if (ObjectUtil.isEmpty(entity.getTodoTime())) {
            return JsonResult.fail("参数 todoTime 不能为空");
        }
        if (!this.saveOrUpdate(entity)) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdEntity(Integer id) {
        if (!this.removeById(id)) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdsEntity(List<Integer> ids) {
        for (Integer id : ids) {
            JsonResult<?> jsonResult = removeByIdEntity(id);
            if (!JsonResult.SUCCESS.equals(jsonResult.getState())) {
                return JsonResult.fail();
            }
        }
        return JsonResult.ok();
    }

    @Override
    public JsonResult<?> preparedData() {
        Map<String, Object> data = new HashMap<>();
        // 示例
        List<Map<String, Object>> isFinishSelectList = EnumUtil.getSelectList(IsEnum.class);
        data.put("isFinishSelectList", isFinishSelectList);
        return JsonResult.ok(data);
    }

    @Override
    public JsonResult<?> toggleIsFinish(Integer id, IsEnum isFinish) {
        if (ObjectUtil.isEmpty(id)) {
            return JsonResult.fail("缺少参数 id");
        }
        if (ObjectUtil.isEmpty(isFinish)) {
            return JsonResult.fail("缺少参数 isFinish");
        }
        Todo todoEntity = this.getById(id);
        if (ObjectUtil.isEmpty(todoEntity)) {
            return JsonResult.fail("该条数据不存在");
        }
        todoEntity.setIsFinish(isFinish);
        if (!this.saveOrUpdate(todoEntity)) {
            throw new ServiceException("修改数据失败");
        }
        return JsonResult.ok();
    }

    @Override
    public List<Todo> listTodayWaitDo() {
        LambdaQueryWrapper<Todo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Todo::getTodoUserId, UserChacheFromToken.getUserId());
        queryWrapper.le(Todo::getTodoTime, DateUtil.endOfDay(new Date()));
        queryWrapper.eq(Todo::getIsFinish, IsEnum.NO);
        queryWrapper.orderByAsc(Todo::getTodoTime);
        return this.list(queryWrapper);
    }

}

