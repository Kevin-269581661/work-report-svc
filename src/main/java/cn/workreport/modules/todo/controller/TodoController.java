package cn.workreport.modules.todo.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.todo.entity.Todo;
import cn.workreport.modules.todo.service.ITodoService;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import cn.workreport.util.UserChacheFromToken;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 待办 对应控制器
 * </p>
 *
 * @author yyf
 * @since 2022-05-11
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "待办模块")
public class TodoController extends BaseController {

    @Autowired
    private ITodoService todoService;

    /**
     * 保存或修改待办
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存待办")
    @PostMapping("/todo/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(Todo entity) {
        log.info("===> 进入 /todo/saveOrUpdate ===> params: entity=" + entity);
        return todoService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询待办
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询待办")
    @PostMapping("/todo/listPage")
    public JsonResult<?> listPage(@RequestBody PagePO<Todo> pagePO) {
        log.info("===> 进入 /todo/listPage ===> params: pagePO=" + pagePO);
        IPage<Todo> page = new PageQueryUtil<Todo>().getPage(pagePO);
        LambdaQueryWrapper<Todo> wrapper = new LambdaQueryWrapper<>();
        Todo condition = pagePO.getCondition();
        if (ObjectUtil.isNotEmpty(condition)) {
            wrapper.ge(ObjectUtil.isNotEmpty(condition.getStartDateTime()), Todo::getTodoTime, condition.getStartDateTime());
            wrapper.le(ObjectUtil.isNotEmpty(condition.getEndDateTime()), Todo::getTodoTime, condition.getEndDateTime());
            wrapper.eq(ObjectUtil.isNotEmpty(condition.getIsFinish()), Todo::getIsFinish, condition.getIsFinish());
        }
        wrapper.eq(Todo::getTodoUserId, UserChacheFromToken.getUserId());
        wrapper.orderByDesc(Todo::getTodoTime, Todo::getCreatedTime);
        PageVO<Todo> result = todoService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询待办
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询待办")
    @GetMapping("/todo/getById")
    public JsonResult<?> getById(Integer id) {
        log.info("===> 进入 /todo/getById ===> params: id=" + id);
        if (ObjectUtils.isEmpty(id)) {
            return JsonResult.fail("参数 id 不能为空！");
        }
        Todo entity = todoService.getByIdEntity(id);
        return JsonResult.ok(entity);
    }

    /**
     * 批量或单体删除待办数据
     *
     * @param idsPO ID集合
     * @return 成功与否
     */
    @ApiOperation("批量删除待办")
    @PostMapping("/todo/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO) {
        log.info("===> 进入 /todo/removeByIds ===> params: idsPO=" + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("参数 ids 不能为空！");
        }
        return todoService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 待办预加载数据
     *
     * @return 预加载数据列表的 Map
     */
     @ApiOperation("待办预加载数据")
     @GetMapping("/todo/preparedData")
     public JsonResult<?> preparedData() {
       log.info("===> 进入 /todo/preparedData ===>");
       return todoService.preparedData();
     }

    /**
     * 设置待办是否完成
     *
     * @param id 待办id
     * @param isFinish 是否完成
     * @return JsonResult
     */
    @ApiOperation("设置待办是否完成")
    @PostMapping("/todo/toggleIsFinish")
    public JsonResult<?> toggleIsFinish(Integer id, IsEnum isFinish) {
        log.info("===> 进入 /todo/toggleIsFinish ===>");
        return todoService.toggleIsFinish(id, isFinish);
    }

    /**
     * 查询今日待办
     *
     * @return 查询结果
     */
    @ApiOperation("查询今日待办")
    @GetMapping("/todo/listTodayWaitDo")
    public JsonResult<?> listTodayWaitDo() {
        log.info("===> 进入 /todo/listTodayWaitDo ===>" );
        List<Todo> result = todoService.listTodayWaitDo();
        return JsonResult.ok(result);
    }

}
