package cn.workreport.modules.upload.mapper;

import cn.workreport.modules.upload.entity.Upload;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件上传 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-19
 */
@Mapper
public interface UploadMapper extends BaseMapper<Upload> {

}
