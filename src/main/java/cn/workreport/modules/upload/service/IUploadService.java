package cn.workreport.modules.upload.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.upload.entity.Upload;
import cn.workreport.modules.upload.enums.BusinessKeyEnum;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 文件上传 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-19
 */
public interface IUploadService extends BaseService<Upload> {

    /**
     * 解析base64成文件，压缩并保存文件，返回保存信息对象 Upload
     *
     * @param base64Str base64 字符串
     * @param realPath  存储文件的根目录路径
     * @param fileDir   文件夹-相对路径
     * @param suffix    文件的后缀 如: ".jpeg" （png 的文件不支持压缩！！！）
     * @return Upload
     */
    Upload base64TransferImgEntity(
            String base64Str,
            String realPath,
            String fileDir,
            String suffix,
            BusinessKeyEnum businessKey
    );

    /**
     * 上传图片文件
     *
     * @param file        文件
     * @param businessKey 业务名
     * @param type        类型
     * @return Upload
     */
    Upload uploadImage(MultipartFile file, BusinessKeyEnum businessKey, String type);
}

