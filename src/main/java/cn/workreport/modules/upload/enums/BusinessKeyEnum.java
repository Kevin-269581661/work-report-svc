package cn.workreport.modules.upload.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 上传业务名枚举
 */
@AllArgsConstructor
public enum BusinessKeyEnum implements ParamEnum {

    /**
     * 头像
     */
    AVATAR("1", "头像"),

    /**
     * 待办事项
     */
    TODO("2", "待办事项"),

    /**
     * 任务
     */
    TASK("3", "任务");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static BusinessKeyEnum getEntityByVal (String value) {
        for (BusinessKeyEnum entity : BusinessKeyEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
