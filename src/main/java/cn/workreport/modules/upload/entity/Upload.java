package cn.workreport.modules.upload.entity;

import cn.workreport.modules.upload.enums.BusinessKeyEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 文件上传
 * </p>
 *
 * @author yyf
 * @since 2022-01-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_upload")
@ApiModel(value = "Upload对象", description = "文件上传")
public class Upload extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("文件名（包括后缀）")
    private String fileName;

    @ApiModelProperty("文件相对路径（包括文件名）")
    private String filePath;

    @ApiModelProperty("文件位置")
    private String fileLocation;

    @ApiModelProperty("文件大小（字节）")
    private Long fileSize;

    @ApiModelProperty("业务名")
    private BusinessKeyEnum businessKey;

    @ApiModelProperty("类型")
    private String type;

//--------------------------------------以下为非表字段--------------------------------------------------

//--------------------------------------以上为非表字段---------------------------------------------
}
