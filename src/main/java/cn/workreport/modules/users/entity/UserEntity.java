package cn.workreport.modules.users.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.enums.UserRoleEnum;
import cn.workreport.modules.users.enums.SexEnum;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表对应的实体类
 * @version 1.0
 * @since 1.0
 * @see BaseEntity
 * @JsonInclude 注解：
 * 标注应该返回的json中包含哪些属性
 * 可以在propertis文件中全局设置
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserEntity", description = "用户表对应的实体类")
@TableName("t_user")
public class UserEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1226136407883386580L;

    @ApiModelProperty("用户名")
    @TableField
    private String username;

    @ApiModelProperty("用户密码")
    @TableField
    private String password;

    @ApiModelProperty("密码盐值")
    @TableField
    private String salt;

    @ApiModelProperty("昵称")
    @TableField
    private String nickname;

    @ApiModelProperty("性别：0-女，1-男")
    @TableField
    private SexEnum gender;

    @ApiModelProperty("电话号码")
    @TableField
    private String phone;

    @ApiModelProperty("电子邮箱")
    @TableField
    private String email;

    @ApiModelProperty("头像")
    @TableField
    private String avatar;

    @ApiModelProperty("是否删除：0-未删除，1-已删除")
    @TableField
    private IsEnum deleted;

    @ApiModelProperty("登录的token")
    @TableField
    private String token;

    @ApiModelProperty("角色id")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer roleId;

    @ApiModelProperty("姓名")
    @TableField
    private String truename;

    @ApiModelProperty("所属组的id")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer groupId;

    @ApiModelProperty("是否是组织的组长")
    @TableField(value = "is_leader", updateStrategy = FieldStrategy.IGNORED)
    private Boolean isLeader;

    @ApiModelProperty("所属机构的id")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer orgId;

    @ApiModelProperty("简介")
    @TableField
    private String description;

    @ApiModelProperty("上次登录ip")
    @TableField
    private String lastLoginIp;

    @ApiModelProperty("上次登录地区")
    @TableField
    private String lastLoginArea;

    @ApiModelProperty("上次登录时间")
    @TableField
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    @ApiModelProperty("是否是超级管理员")
    @TableField("is_supper_admin")
    private Boolean isSupperAdmin;

    @ApiModelProperty("绑定机构的时间（最新的一次）")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField
    private Date bindOrgTime;

    @ApiModelProperty("是否开启邮箱提醒")
    @TableField("is_open_email_notation")
    private Boolean isOpenEmailNotation;

    /*    ----------------------- 以下为非表字段 ---------------------------     */
    @TableField(exist = false)
    private String genderLabel;

    @TableField(exist = false)
    private String deletedLabel;

    /**
     * 角色名称
     */
    @TableField(exist = false)
    private String roleName;

    /**
     * 机构名称
     */
    @TableField(exist = false)
    private String orgName;

    /**
     * 组织名称
     */
    @TableField(exist = false)
    private String groupName;

}
