package cn.workreport.modules.users.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 用户补充信息对应实体类
 * </p>
 *
 * @author yyf
 * @since 2022-04-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_user_extends")
@ApiModel(value = "UserExtends", description = "用户补充信息对应实体类")
public class UserExtends extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户id")
    @TableField
    private Integer userId;

    @ApiModelProperty("绑定机构的时间（最新的一次）")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField
    private Date bindOrgTime;

// --------------------------------------以下为非表字段-------------------------------------------- //

// --------------------------------------以上为非表字段-------------------------------------------- //
}
