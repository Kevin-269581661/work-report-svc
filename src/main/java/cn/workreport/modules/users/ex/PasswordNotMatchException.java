package cn.workreport.modules.users.ex;

import cn.workreport.modules.common.exception.ServiceException;

/**
 * 用户密码不正确的异常
 */
public class PasswordNotMatchException extends ServiceException {
    private static final long serialVersionUID = 7052733251567021480L;

    public PasswordNotMatchException() {
        super();
    }

    public PasswordNotMatchException(String message) {
        super(message);
    }

    public PasswordNotMatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordNotMatchException(Throwable cause) {
        super(cause);
    }

    protected PasswordNotMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
