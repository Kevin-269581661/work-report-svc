package cn.workreport.modules.users.ex;

import cn.workreport.modules.common.exception.ServiceException;

/**
 * 用户名未找到异常
 */
public class UserNotFoundException extends ServiceException {
    private static final long serialVersionUID = -3089148097322323079L;

    public UserNotFoundException() {
        super();
    }

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotFoundException(Throwable cause) {
        super(cause);
    }

    protected UserNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
