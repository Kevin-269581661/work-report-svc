package cn.workreport.modules.users.ex;

import cn.workreport.modules.common.exception.ServiceException;

/**
 * 用户名冲突的异常（例如：注册时，用户名被占用）
 */
public class UsernameDuplicateException extends ServiceException {
    private static final long serialVersionUID = 3164055183124220212L;

    public UsernameDuplicateException() {
        super();
    }

    public UsernameDuplicateException(String message) {
        super(message);
    }

    public UsernameDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameDuplicateException(Throwable cause) {
        super(cause);
    }

    protected UsernameDuplicateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
