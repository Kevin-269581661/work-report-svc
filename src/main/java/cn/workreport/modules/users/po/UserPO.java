package cn.workreport.modules.users.po;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.enums.UserRoleEnum;
import cn.workreport.modules.users.enums.SexEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * 接收客户端传来参数的对象
 */
@Data
public class UserPO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String username; // "用户名"

    private String password; // "用户密码"

    private String nickname; // "昵称"

    private SexEnum gender;// "性别：0-女，1-男"

    private String phone;// "电话号码"

    private String email; // "电子邮箱"

    private String avatar; // "头像"

    private Integer roleId; // 角色id

    private String roleName; // 角色名称

    private UserRoleEnum role; // 用户角色

    private IsEnum deleted; // "是否删除：0-未删除，1-已删除"

    private String truename; // 姓名

    private Integer groupId; // 所属组的id

    private Integer orgId; // 所属机构的id

    private String description; // 简介

    private Boolean isOpenEmailNotation;

}
