package cn.workreport.modules.users.po;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.enums.UserRoleEnum;
import cn.workreport.modules.users.enums.SexEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 接收客户端传来参数的对象
 */
@Data
public class UserIdsPO implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Integer> userIds;

}
