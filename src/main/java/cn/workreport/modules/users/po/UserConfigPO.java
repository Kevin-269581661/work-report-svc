package cn.workreport.modules.users.po;

import lombok.Data;

import java.io.Serializable;

/**
 *  用户配置信息
 */
@Data
public class UserConfigPO implements Serializable {
    private static final Long serialVersionUID = 1L;

    // 是否开启邮箱提醒
    private Boolean isOpenEmailNotation;

}
