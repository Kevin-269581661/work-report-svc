package cn.workreport.modules.users.mapper;

import cn.workreport.modules.users.entity.UserExtends;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户补充信息 Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-04-02
 */
@Mapper
public interface UserExtendsMapper extends BaseMapper<UserExtends> {

}
