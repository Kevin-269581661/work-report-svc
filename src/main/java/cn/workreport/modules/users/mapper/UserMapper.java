package cn.workreport.modules.users.mapper;

import cn.workreport.modules.users.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 处理用户数据持久层接口
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    /**
     * 根据用户名查询用户数据
     * @param username 用户名
     * @return 匹配的用户数据，如果没有则返回 null
     */
    UserEntity findByUserName(@Param("username") String username);

    IPage<UserEntity> queryUsers(Page<UserEntity> page);
}
