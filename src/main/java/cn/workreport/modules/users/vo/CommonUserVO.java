package cn.workreport.modules.users.vo;

import cn.workreport.modules.users.enums.SexEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回给客户端通用用户数据对象
 */
@Data
public class CommonUserVO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Integer id;

    private String username; // "用户名"

    private String nickname; // "昵称"

    private SexEnum gender;// "性别：0-女，1-男"

    private String phone;// "电话号码"

    private String email; // "电子邮箱"

    private String avatar; // "头像"

    private String truename; // 姓名

    private Integer groupId; // 所属组的id

    private Boolean isLeader; // 是否是组织的组长

    private Integer orgId; // 所属机构的id

    private String orgName; // 所属机构的名称

}
