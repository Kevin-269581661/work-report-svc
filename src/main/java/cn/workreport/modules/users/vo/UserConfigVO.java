package cn.workreport.modules.users.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回给客户端的数据对象 用户配置信息
 */
@Data
public class UserConfigVO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Integer userId;

    // 是否开启邮箱提醒
    private Boolean isOpenEmailNotation;

}
