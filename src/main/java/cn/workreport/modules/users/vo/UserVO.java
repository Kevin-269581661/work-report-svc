package cn.workreport.modules.users.vo;

import cn.workreport.modules.common.enums.UserRoleEnum;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.users.enums.SexEnum;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 返回给客户端的数据对象
 */
@Data
public class UserVO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Integer id;

    private String username; // "用户名"

    private String nickname; // "昵称"

    private SexEnum gender;// "性别：0-女，1-男"

    private String phone;// "电话号码"

    private String email; // "电子邮箱"

    private String avatar; // "头像"

    private String token; // 登录的token

//    private Integer roleId; // 角色id

    private String roleName; // 角色名称

    private String truename; // 姓名

    private Integer groupId; // 所属组的id

    private Boolean isLeader; // 是否是组织的组长

    private Integer orgId; // 所属机构的id

    private String orgName; // 所属机构的名称

    private String description; // 简介

    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime; // 上次登录时间

    private String lastLoginArea; // 上次登录地区

    private Boolean isSupperAdmin; // 是否是超级管理员

    private List<String> permissions; // 所有权限

}
