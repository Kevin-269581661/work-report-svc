package cn.workreport.modules.users.dto;

import cn.workreport.modules.common.enums.UserRoleEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * 中间层传递数据的对象
 */
@Data
public class UserDTO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Integer id;

    private String username; // "用户名"

    private String nickname; // "昵称"

    private Integer gender;// "性别：0-女，1-男"

    private String phone;// "电话号码"

    private String email; // "电子邮箱"

    private String avatar; // "头像"

    private String token; // 登录的token
}
