package cn.workreport.modules.users.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.users.entity.UserExtends;

/**
 * <p>
 * 用户补充信息 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-04-02
 */
public interface IUserExtendsService extends BaseService<UserExtends> {
    /**
     * 用户补充信息预加载数据
     *
     * @return 预加载数据列表的 Map
     */
    // JsonResult<?> preparedData();

    /**
     * 根据 用户id 查询用户补充信息
     * @param userId 用户id
     * @return 用户补充信息
     */
    UserExtends getByUserId(Integer userId);
}

