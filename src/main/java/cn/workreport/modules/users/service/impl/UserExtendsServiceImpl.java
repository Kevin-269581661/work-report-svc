package cn.workreport.modules.users.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.users.entity.UserExtends;
import cn.workreport.modules.users.mapper.UserExtendsMapper;
import cn.workreport.modules.users.service.IUserExtendsService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.util.JsonResult;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户补充信息 业务接口实现类
 * </p>
 *
 * @author yyf
 * @since 2022-04-02
 */
@Slf4j
@Service
public class UserExtendsServiceImpl extends ServiceImpl<UserExtendsMapper, UserExtends> implements IUserExtendsService {

    @Override
    public void wrapEntity(UserExtends entity) {
        if (entity == null) {
            return;
        }
    }

    @Override
    public PageVO<UserExtends> pageEntity(IPage<UserExtends> page, Wrapper<UserExtends> queryWrapper) {
        IPage<UserExtends> pageResult = this.page(page, queryWrapper);
        pageResult.getRecords().forEach(entity -> {
            this.wrapEntity(entity);
        });
        return new PageVO<>(pageResult);
    }

    @Override
    public UserExtends getByIdEntity(Integer id) {
        UserExtends entity = this.getById(id);
        this.wrapEntity(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> saveOrUpdateEntity(UserExtends entity) {
        boolean isSuccess =  this.saveOrUpdate(entity);
        if (!isSuccess) {
            return JsonResult.fail("操作失败");
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdEntity(Integer id) {
        boolean isSuccess = this.removeById(id);
        if (!isSuccess) {
            return JsonResult.fail();
        }
        return JsonResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult<?> removeByIdsEntity(List<Integer> ids) {
        for (Integer id : ids) {
            JsonResult<?> jsonResult = removeByIdEntity(id);
            if (!jsonResult.getState().equals(JsonResult.SUCCESS)) {
                return JsonResult.fail();
            }
        }
        return JsonResult.ok();
    }

    @Override
    public UserExtends getByUserId(Integer userId) {
        if (ObjectUtil.isEmpty(userId)) {
            return null;
        }
        LambdaQueryWrapper<UserExtends> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(UserExtends::getUserId, userId);
        return this.getOne(queryWrapper);
    }

    // @Override
    // public JsonResult<?> preparedData() {
    //     Map<String, Object> data = new HashMap<>();
    //     // 示例
    //     List<Map<String, Object>> deletedSelectList = EnumUtil.getSelectList(IsEnum.class);
    //     data.put("deletedSelectList", deletedSelectList);
    //     return JsonResult.ok(data);
    // }

}

