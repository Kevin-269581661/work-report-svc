package cn.workreport.modules.users.service;

import cn.workreport.modules.common.enums.IsEnum;
import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.users.entity.UserEntity;
import cn.workreport.modules.users.po.UserPO;
import cn.workreport.modules.users.vo.UserVO;
import cn.workreport.util.JsonResult;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 处理用户相关业务的接口
 */
public interface IUserService extends BaseService<UserEntity> {
    /**
     * 用户注册
     *
     * @param user 新用户信息
     */
    JsonResult<?> reg(UserPO user);

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 用户密码
     * @return 用户信息
     */
    JsonResult<?> login(String username, String password, HttpServletRequest request);

    /**
     * 用户修改用户信息
     *
     * @param user user
     * @return JsonResult
     */
    JsonResult<?> update(UserPO user);

    /**
     * 用户修改用户密码
     *
     * @param
     * @return JsonResult
     */
    JsonResult<?> updatePassword(String oldPassword, String newPassword);

    /**
     * 用户修改头像
     *
     * @param
     * @return JsonResult
     */
    JsonResult<?> updateAvatar(String avatar);

    /**
     * 管理员注册用户
     *
     * @param user
     * @return JsonResult
     */
    JsonResult<?> regByAdmin(UserPO user);

    /**
     * 管理员修改用户
     *
     * @param user
     * @return JsonResult
     */
    JsonResult<?> updateByAdmin(UserPO user);

    /**
     * 管理员修改用户角色
     *
     * @param userId userId
     * @param roleId roleId
     * @return JsonResult
     */
    JsonResult<?> updateRoleByAdmin(Integer userId, Integer roleId);

    /**
     * 管理员修改用户重置密码
     *
     * @param userId      userId
     * @param newPassword newPassword
     * @return JsonResult
     */
    JsonResult<?> updatePasswordByAdmin(Integer userId, String newPassword);

    /**
     * 管理员修改用户重置密码
     *
     * @param userId  userId
     * @param deleted deleted
     * @return JsonResult
     */
    JsonResult<?> toggleDeletedByAdmin(Integer userId, IsEnum deleted);

    /**
     * 添加人员到组织
     *
     * @param userIds userIds
     * @param groupId groupId
     * @return JsonResult
     */
    JsonResult<?> updateUsersGroup(List<Integer> userIds, Integer groupId, Integer leaderId, Integer orgId);

    /**
     * 用户绑定机构
     *
     * @param orgId
     * @return JsonResult
     */
    JsonResult<?> userBindOrg(Integer orgId, String verifyAnswer);

    /**
     * 根据角色id查询用户列表
     *
     * @param roleId 角色id
     * @return 用户列表
     */
    List<UserEntity> listByRoleId(Integer roleId);

    /**
     * 根据机构id查询用户列表
     *
     * @param orgId 机构id
     * @return 用户列表
     */
    List<UserEntity> listByOrgId(Integer orgId);

    /**
     * 查询获取没有组织的用户列表
     *
     * @param orgId 机构id
     * @return JsonResult
     */
    JsonResult<?> listHasNotGroupUser(Integer orgId);

    /**
     * 用户列表预加载数据
     *
     * @return JsonResult
     */
    JsonResult<?> preparedData();

    /**
     * 当前登录用户解除关联的机构
     *
     * @param orgId 机构id
     * @return JsonResult
     */
    JsonResult<?> userUnboundOrg(Integer orgId);

    /**
     * 根据 机构id 查询机构人员数量
     *
     * @param orgId 机构id
     * @return 机构人员数量
     */
    Integer countMemberByOrgId(Integer orgId);

    /**
     * 获取前端展示的用户信息
     *
     * @param userEntity
     * @return
     */
    UserVO getVoByEntity(UserEntity userEntity);

    /**
     * 添加组织成员
     *
     * @param userId 用户 id
     * @param groupId 组织的 id
     * @return
     */
    JsonResult<?> userBindGroup(Integer userId, Integer groupId);

    /**
     * 删除组织成员
     *
     * @param userId 用户 id
     * @return
     */
    JsonResult<?> userUnboundGroup(Integer userId);

    /**
     * 设置组织管理员
     *
     * @param userId 用户 id
     * @return
     */
    JsonResult<?> setGroupManager(Integer userId);

    /**
     * 根据组织id获取成员列表
     *
     * @param groupId
     * @return
     */
    List<UserEntity> getUserListByGroupId(Integer groupId);

    /**
     * 根据组长id，查询组员
     *
     * @param id
     * @return
     */
    List<UserEntity> getMembersByLeaderId(Integer id);

    /**
     * 退出登录
     */
    void logout();
}
