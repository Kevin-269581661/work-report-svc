package cn.workreport.modules.users.controller;

import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;

import cn.workreport.modules.users.entity.UserExtends;
import cn.workreport.modules.users.service.IUserExtendsService;

/**
 * <p>
 * 用户补充信息 对应控制器
 * </p>
 *
 * @author yyf
 * @since 2022-04-02
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "用户补充信息模块")
public class UserExtendsController extends BaseController {

    @Autowired
    private IUserExtendsService userExtendsService;

    /**
     * 保存或修改用户补充信息
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存用户补充信息")
    @PostMapping("/userExtends/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(UserExtends entity) {
        log.info("===> 进入 /userExtends/saveOrUpdate ===> params: entity=" + entity);
        return userExtendsService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询用户补充信息
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询用户补充信息")
    @PostMapping("/userExtends/listPage")
    public JsonResult<?> listPage(PagePO<UserExtends> pagePO){
        log.info("===> 进入 /userExtends/listPage ===> params: pagePO=" + pagePO);
        IPage<UserExtends> page = new PageQueryUtil<UserExtends>().getPage(pagePO);
        LambdaQueryWrapper<UserExtends> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(UserExtends::getCreatedTime);
        PageVO<UserExtends> result = userExtendsService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询用户补充信息
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询用户补充信息")
    @GetMapping("/userExtends/getById")
    public JsonResult<?> getById(Integer id){
        log.info("===> 进入 /userExtends/getById ===> params: id=" + id);
        if (ObjectUtils.isEmpty(id)) {
            return JsonResult.fail("参数 id 不能为空！");
        }
        UserExtends entity = userExtendsService.getByIdEntity(id);
        return JsonResult.ok(entity);
    }

    /**
    * 批量或单体删除用户补充信息数据
    *
    * @param idsPO ID集合
    * @return 成功与否
    */
    @ApiOperation("批量删除用户补充信息")
    @PostMapping("/userExtends/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO){
        log.info("===> 进入 /userExtends/removeByIds ===> params: idsPO=" + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("参数 ids 不能为空！");
        }
        return userExtendsService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 用户补充信息预加载数据
     *
     * @return 预加载数据列表的 Map
     */
    // @ApiOperation("用户补充信息预加载数据")
    // @GetMapping("/userExtends/preparedData")
    // public JsonResult<?> preparedData() {
        //   log.info("===> 进入 /userExtends/preparedData ===>");
        //   return userExtendsService.preparedData();
    // }

}
