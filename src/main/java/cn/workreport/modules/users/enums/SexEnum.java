package cn.workreport.modules.users.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 性别：0-女，1-男
 */
@AllArgsConstructor
public enum SexEnum implements ParamEnum {

    /**
     * 男
     */
    YES("1", "男"),

    /**
     * 女
     */
    NO("0", "女");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static SexEnum getEntityByVal (String value) {
        for (SexEnum entity : SexEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
