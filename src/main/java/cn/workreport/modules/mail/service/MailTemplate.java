package cn.workreport.modules.mail.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.Date;
import java.util.Map;

/**
 * 描述【邮件发送实现】
 *
 * @author lihuanyao
 * @date 2022/4/26 9:41
 */
@Slf4j
@Service
public class MailTemplate {
    @Autowired
    JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String fromMail;

    /**
     * 发送邮件
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param ccAddress     抄送地址
     * @param bccAddress    密送地址
     */
    public void sendSimpleMail(String toMailAddress, String title, String text, String ccAddress, String bccAddress) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(title);
        message.setFrom(fromMail);
        message.setTo(toMailAddress);
        if (StringUtils.isNotBlank(ccAddress)) {
            message.setCc(ccAddress);
        }
        if (StringUtils.isNotBlank(bccAddress)) {
            message.setBcc(bccAddress);
        }
        message.setSentDate(new Date());
        message.setText(text);
        javaMailSender.send(message);
    }

    /**
     * 发送邮件
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     */
    public void sendSimpleMail(String toMailAddress, String title, String text) {
        sendSimpleMail(toMailAddress, title, text, null, null);
    }

    /**
     * 发送邮件  (支持富文本)
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param ccAddress     抄送地址
     * @param bccAddress    密送地址
     */
    public void sendTextMail(String toMailAddress, String title, String text, String ccAddress, String bccAddress) {
        sendMail(toMailAddress, title, text, ccAddress, bccAddress, null, null);
    }

    /**
     * 发送邮件 (支持富文本)
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     */
    public void sendTextMail(String toMailAddress, String title, String text) {
        sendTextMail(toMailAddress, title, text, null, null);
    }


    /**
     * 发送邮件（全部配置）
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param ccAddress     抄送地址
     * @param bccAddress    密送地址
     * @param imgMap        图片集合 支持 File InputStreamSource DataSource  Resource 四种文件流格式
     * @param fileMap       附件集合 支持 File InputStreamSource DataSource 三种文件流格式
     */
    private void sendMail(String toMailAddress,
                          String title,
                          String text,
                          String ccAddress,
                          String bccAddress,
                          Map<String, Object> imgMap,
                          Map<String, Object> fileMap) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            helper.setSubject(title);
            helper.setFrom(fromMail);
            helper.setTo(toMailAddress);
            if (StringUtils.isNotBlank(ccAddress)) {
                helper.setCc(ccAddress);
            }
            if (StringUtils.isNotBlank(bccAddress)) {
                helper.setBcc(bccAddress);
            }
            helper.setSentDate(new Date());
            // 设置html解析
            helper.setText(text,true);
            //添加图片文件
            //使用范例
            /**
             *  模板位置： <p>第一张图片：</p><img src='cid:p01'/>
             *  helper.addInline("p01",new FileSystemResource(new File("C:\\Users\\Administrator\\Desktop\\11.jpg")));
             */
            if (imgMap != null && !imgMap.isEmpty()) {
                for (Map.Entry<String, Object> entry : imgMap.entrySet()) {
                    Object value = entry.getValue();
                    if (value != null) {
                        if (value instanceof File) {
                            helper.addInline(entry.getKey(), (File) entry.getValue());
                        } else if (value instanceof Resource) {
                            helper.addInline(entry.getKey(), (Resource) entry.getValue());
                        } else if (value instanceof DataSource) {
                            helper.addInline(entry.getKey(), (DataSource) entry.getValue());
                        } else if (value instanceof InputStreamSource) {
                            helper.addInline(entry.getKey(), (InputStreamSource) entry.getValue(), "application/octet-stream");
                        }
                    }

                }
            }
            //添加附件
            if (fileMap != null && !fileMap.isEmpty()) {
                for (Map.Entry<String, Object> entry : fileMap.entrySet()) {
                    Object value = entry.getValue();
                    if (value != null) {
                        if (value instanceof File) {
                            helper.addAttachment(entry.getKey(), (File) entry.getValue());
                        } else if (value instanceof InputStreamSource) {
                            helper.addAttachment(entry.getKey(), (InputStreamSource) entry.getValue());
                        } else if (value instanceof DataSource) {
                            helper.addAttachment(entry.getKey(), (DataSource) entry.getValue());
                        }
                    }

                }
            }
            javaMailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error("邮件发送失败！");
        }
    }

    /**
     * 发送邮件（全部配置）
     *
     * @param toMailAddress
     * @param title
     * @param text
     * @param imgMap
     * @param fileMap
     */
    private void sendMail(String toMailAddress,
                          String title,
                          String text,
                          Map<String, Object> imgMap,
                          Map<String, Object> fileMap) {
        sendMail(toMailAddress, title, text, null, null, imgMap, fileMap);
    }

    /**
     * 发送邮件 （带附件）
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param ccAddress     抄送地址
     * @param bccAddress    密送地址
     * @param fileMap       文件集合 支持 File InputStreamSource DataSource 三种文件流格式
     */
    public void sendAttachFileMail(String toMailAddress,
                                   String title,
                                   String text,
                                   String ccAddress,
                                   String bccAddress,
                                   Map<String, Object> fileMap) {
        sendMail(toMailAddress, title, text, ccAddress, bccAddress, null, fileMap);
    }

    /**
     * 发送邮件 （带附件）
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param fileMap       文件集合
     */
    public void sendAttachFileMail(String toMailAddress, String title, String text, Map<String, Object> fileMap) {
        sendAttachFileMail(toMailAddress, title, text, null, null, fileMap);
    }

    /**
     * 发送邮件 （带附件）
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param ccAddress     抄送地址
     * @param bccAddress    密送地址
     * @param imgMap        图片集合 支持 File InputStreamSource DataSource  Resource 四种文件流格式
     */
    public void sendImgMail(String toMailAddress,
                            String title,
                            String text,
                            String ccAddress,
                            String bccAddress,
                            Map<String, Object> imgMap) {
        sendMail(toMailAddress, title, text, ccAddress, bccAddress, imgMap, null);
    }

    /**
     * 发送邮件 （带附件）
     *
     * @param toMailAddress 寄往邮件地址
     * @param title         主题
     * @param text          正文
     * @param imgMap        图片集合 支持 File InputStreamSource DataSource  Resource 四种文件流格式
     */
    public void sendImgMail(String toMailAddress, String title, String text, Map<String, Object> imgMap) {
        sendImgMail(toMailAddress, title, text, null, null, imgMap);
    }


}
