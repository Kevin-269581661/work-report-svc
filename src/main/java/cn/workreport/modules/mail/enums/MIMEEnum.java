package cn.workreport.modules.mail.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 描述【MIME 协议相关枚举】
 *
 * @author lihuanyao
 * @date 2022/4/26 11:22
 */
@AllArgsConstructor
public enum MIMEEnum {

    PNG("image/png","png"),
    BMP("image/bmp","bmp/dib"),
    JPG("image/ipeg","jpg/jpeg/jpg"),
    GIF("image/gif","gif"),
    MP3("audio/mpeg","mp3"),
    MP4("video/mp4","mp4/mpg4/m4v/mp4v"),
    PDF("application/pdf","pdf"),
    TEXT("text/plan","text/txt"),
    JSON("application/json","json"),
    XML("text/xml","xml"),
    DOC("application/msword","doc"),
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document","xls"),
    XLS("application/vnd.ms-excel","docx"),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","xlsx"),
    // 如不清楚类型默认以二进制类型
    other("application/octet-stream","other");

    @Getter
    private String code;
    @Getter
    private String lable;

}
