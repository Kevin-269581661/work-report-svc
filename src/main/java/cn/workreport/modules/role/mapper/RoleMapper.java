package cn.workreport.modules.role.mapper;

import cn.workreport.modules.role.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
