package cn.workreport.modules.role.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.role.entity.Role;
import cn.workreport.util.JsonResult;

import java.util.List;

/**
 * <p>
 * 业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
public interface IRoleService extends BaseService<Role> {

    /**
     * 设置启用或停用
     *
     * @param id
     * @param asUse
     * @return
     */
    JsonResult<?> toggleUse(Integer id, Boolean asUse);

    /**
     * 根据名称查询该条数据
     *
     * @param roleName
     * @return
     */
    Role getEntityByName(String roleName, Integer orgId);

    /**
     * 查询已启用的所有角色列表
     *
     * @return
     */
    List<Role> getEnableList(Integer orgId);

    /**
     * 获取初始化的角色名列表
     *
     * @return 初始化的角色名列表
     */
    List<String> listInitRoleName();

    /**
     * 根据 机构id 查询所有角色
     *
     * @param orgId 机构id
     * @return
     */
    List<Role> listByOrgId(Integer orgId);
}

