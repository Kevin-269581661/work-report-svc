package cn.workreport.modules.role.entity;

import cn.workreport.modules.permission.enums.PermissionEnum;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 角色表对应实体类
 * </p>
 *
 * @author yyf
 * @since 2022-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "t_role", autoResultMap = true)
@ApiModel(value = "Role", description = "角色表对应实体类")
public class Role extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("角色名称")
    @TableField
    private String roleName;

    @ApiModelProperty("所属机构")
    @TableField
    private Integer orgId;

    @ApiModelProperty("包含的权限编码集合")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> permissions;

    @ApiModelProperty("是否使用")
    @TableField("is_use")
    private Boolean asUse;

//--------------------------------------以下为非表字段--------------------------------------------------
    /**
     * 机构名称
     */
    @TableField(exist = false)
    private String orgName;
//--------------------------------------以上为非表字段---------------------------------------------
}
