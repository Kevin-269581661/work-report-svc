package cn.workreport.modules.cascade.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 级联关系
 */
@Data
@ApiModel(value = "CascadeRelationEntity 对象", description = "模块级联关系列表")
@TableName("t_cascade_relation")
public class CascadeRelationEntity extends BaseEntity implements Serializable {
    private static final Long serialVersionUID = 1L;

    @ApiModelProperty("级联项的id")
    @TableField
    private Integer cascadeId;

    @ApiModelProperty("父级id")
    @TableField
    private Integer parentId;

    @ApiModelProperty("子级id")
    @TableField
    private Integer childrenId;

}
