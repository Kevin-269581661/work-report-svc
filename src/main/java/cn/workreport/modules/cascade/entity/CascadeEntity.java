package cn.workreport.modules.cascade.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 级联列表
 */
@Data
@ApiModel(value = "CascadeEntity 对象", description = "模块级联列表")
@TableName("t_cascade")
public class CascadeEntity extends BaseEntity implements Serializable {
    private static final Long serialVersionUID = 1L;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @TableField
    private String name; //

    /**
     * 排序编号 以10位为一个阶梯
     */
    @ApiModelProperty("排序编号")
    @TableField
    private Integer orderNumber;

    /**
     * 机构 id
     */
    @ApiModelProperty("机构 id")
    @TableField
    private Integer orgId;

}
