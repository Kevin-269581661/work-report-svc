package cn.workreport.modules.cascade.mapper;

import cn.workreport.modules.cascade.entity.CascadeEntity;
import cn.workreport.modules.cascade.entity.CascadeRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CascadeRelationMapper extends BaseMapper<CascadeEntity> {

    /**
     * 插入级联关系数据
     * @param cascadeRelationEntity
     * @return
     */
    Integer insertCascadeRelation(CascadeRelationEntity cascadeRelationEntity);

    /**
     * 删除级联关系数据
     * @param cascadeId
     * @return
     */
    Integer deleteCascadeRelationByCascadeId(Integer cascadeId);

    /**
     * 清空跟 cascadeId 相关的 子级 关系
     * @param cascadeId
     * @return
     */
    Integer clearChildrenRelationByCascadeId(Integer cascadeId);

    /**
     * 清空跟 cascadeId 相关的 父级  关系
     * @param cascadeId
     * @return
     */
    Integer clearParentRelationByCascadeId(Integer cascadeId);

    /**
     * 修改子级id
     * @param cascadeId 要修改的，父级的id
     * @param childrenId 父级的 childrenId 修改成为的id
     * @return
     */
    Integer updateChildrenIdById(
            @Param("cascadeId") Integer cascadeId,
            @Param("childrenId") Integer childrenId
    );
}
