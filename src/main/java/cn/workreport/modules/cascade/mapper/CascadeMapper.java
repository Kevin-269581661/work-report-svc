package cn.workreport.modules.cascade.mapper;

import cn.workreport.modules.cascade.entity.CascadeEntity;
import cn.workreport.modules.cascade.vo.CascadeVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CascadeMapper extends BaseMapper<CascadeEntity> {

    /**
     * 查出级联项
     *
     * @param name       名称
     * @param parentId   父级id
     * @param childrenId 子级id
     * @return
     */
    CascadeVO findOne(
            @Param("name") String name,
            @Param("parentId") Integer parentId,
            @Param("childrenId") Integer childrenId
    );

    CascadeVO findOneById(@Param("id") Integer id);

    /**
     * 根据父级id查找出级联列表
     *
     * @param parentId
     * @return
     */
    List<CascadeVO> findListByParentId(@Param("orgId") Integer orgId, @Param("parentId") Integer parentId);

    /**
     * 根据 机构id 查询所有模块
     * @param orgId 机构id
     * @return 模块列表
     */
    List<CascadeVO> listByOrgId(@Param("orgId") Integer orgId);

    /**
     * 根据父级id找出子级最大排序数
     *
     * @param parentId
     * @return
     */
    Integer findMaxOrderNumberByParentId(@Param("parentId") Integer parentId);

    /**
     * 插入级联数据
     *
     * @param cascadeEntity
     * @return
     */
    Integer insertCascade(CascadeEntity cascadeEntity);

    /**
     * 删除级联数据
     *
     * @return
     */
    Integer deleteCascadeById(Integer id);

}
