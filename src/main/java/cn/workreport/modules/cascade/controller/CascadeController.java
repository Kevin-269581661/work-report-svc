package cn.workreport.modules.cascade.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.cascade.dto.CascadeDTO;
import cn.workreport.modules.cascade.service.ICascadeService;
import cn.workreport.modules.cascade.vo.CascadeVO;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.util.JsonResult;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "项目级联模块")
public class CascadeController extends BaseController {

    @Autowired
    private ICascadeService cascadeService;

    /**
     * 保存
     */
    @UserLoginToken(required = true, permission = {PermissionEnum.CASCADE_SAVE_OR_UPDATE})
    @PostMapping("/cascade/save")
    public JsonResult<?> save(CascadeDTO cascadeDTO) {
        log.info("=== 进入 /cascade/save ===> params: cascadeDTO=" + cascadeDTO);
        return cascadeService.saveCascade(cascadeDTO);
    }

    /**
     * 查询
     */
    @UserLoginToken(permission = {PermissionEnum.CASCADE_PAGE})
    @GetMapping("/cascade/list")
    public JsonResult<?> list(Integer orgId, Integer parentId) {
        log.info("=== 进入 /cascade/list ===> params: parentId=" + parentId + ", orgId=" + orgId);
        if (ObjectUtil.isEmpty(orgId)) {
            return JsonResult.fail("缺少参数 orgId");
        }
        List<CascadeVO> result = cascadeService.listCascade(orgId, parentId);
        return JsonResult.ok(result);
    }

    /**
     * 查询树形结构
     */
    @UserLoginToken(permission = {PermissionEnum.CASCADE_PAGE})
    @GetMapping("/cascade/listTree")
    public JsonResult<?> listTree(Integer orgId) {
        log.info("=== 进入 /cascade/listTree ===> params: orgId=" + orgId);
        if (ObjectUtil.isEmpty(orgId)) {
            return JsonResult.fail("缺少参数 orgId");
        }
        return cascadeService.listTree(orgId);
    }

    /**
     * 根据机构查询所有的模块列表
     */
    @UserLoginToken(permission = {PermissionEnum.CASCADE_PAGE})
    @GetMapping("/cascade/listAll")
    public JsonResult<?> listAll(Integer orgId) {
        log.info("=== 进入 /cascade/listAll ===> params: orgId=" + orgId);
        if (ObjectUtil.isEmpty(orgId)) {
            return JsonResult.fail("缺少参数 orgId");
        }
        List<CascadeVO> result = cascadeService.listAll(orgId);
        return JsonResult.ok(result);
    }

    /**
     * 删除
     */
    @UserLoginToken(required = true, permission = {PermissionEnum.CASCADE_DELETE})
    @PostMapping("/cascade/delete")
    public JsonResult<?> delete(Integer id) {
        log.info("=== 进入 /cascade/delete ===> params: id=" + id);
        if (ObjectUtil.isEmpty(id)) {
            return JsonResult.fail("缺少参数 id");
        }
        return cascadeService.removeByIdEntity(id);
    }

}
