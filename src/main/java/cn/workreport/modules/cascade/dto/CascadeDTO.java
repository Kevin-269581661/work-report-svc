package cn.workreport.modules.cascade.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CascadeDTO implements Serializable {
    private static final Long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序编号 以10位为一个阶梯
     */
    private Integer orderNumber;

    /**
     * 父级id
     */
    private Integer parentId;

    /**
     * 子级id
     */
    private Integer childrenId;

    /**
     * 机构id
     */
    private Integer orgId;
}
