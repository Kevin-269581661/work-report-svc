package cn.workreport.modules.cascade.service;

import cn.workreport.modules.cascade.dto.CascadeDTO;
import cn.workreport.modules.cascade.entity.CascadeEntity;
import cn.workreport.modules.cascade.vo.CascadeVO;
import cn.workreport.modules.common.service.BaseService;
import cn.workreport.util.JsonResult;
import cn.workreport.util.R;

import java.util.List;

/**
 * 级联选择模块
 */
public interface ICascadeService extends BaseService<CascadeEntity> {

    /**
     * 保存
     * @param cascadeDTO
     * @return
     */
    JsonResult<?> saveCascade(CascadeDTO cascadeDTO);

    /**
     * 列表
     * @param parentId 父级模块id
     * @return
     */
    List<CascadeVO> listCascade(Integer orgId, Integer parentId);

    /**
     * 查询树形结构
     *
     * @param orgId 机构id
     * @return
     */
    JsonResult<?> listTree(Integer orgId);

    /**
     * 根据机构查询所有的模块列表
     * @param orgId 机构id
     * @return
     */
    List<CascadeVO> listAll(Integer orgId);
}
