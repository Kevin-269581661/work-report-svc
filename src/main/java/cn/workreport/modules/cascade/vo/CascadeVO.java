package cn.workreport.modules.cascade.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CascadeVO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序编号 以10位为一个阶梯
     */
    private Integer orderNumber;

    /**
     * 父级id
     */
    private Integer parentId;

    /**
     * 子级id
     */
    private Integer childrenId;

    /**
     * 机构id
     */
    private String orgId;

    @TableField(exist = false)
    private List<CascadeVO> children;

}
