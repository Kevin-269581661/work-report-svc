package cn.workreport.modules.message.entity;

import cn.workreport.modules.common.entity.BaseEntity;
import cn.workreport.modules.message.enums.MessageLevelEnum;
import cn.workreport.modules.message.enums.MessageTypeEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author yyf
 * @since 2022-02-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_message")
@ApiModel(value = "Message对象", description = "")
public class Message extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField
    @ApiModelProperty("消息类型")
    private MessageTypeEnum type;

    @TableField
    @ApiModelProperty("是否已读")
    private Boolean isRead;

    @TableField
    @ApiModelProperty("消息标题")
    private String title;

    @TableField
    @ApiModelProperty("消息内容")
    private String content;

    @TableField
    @ApiModelProperty("接收人id")
    private Integer receiveUserId;

    @TableField
    @ApiModelProperty("消息等级")
    private MessageLevelEnum level;

//--------------------------------------以下为非表字段--------------------------------------------------
    @TableField(exist = false)
    @ApiModelProperty("消息类型中文")
    private String typeLabel;
//--------------------------------------以上为非表字段---------------------------------------------
}
