package cn.workreport.modules.message.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息类型
 */
@AllArgsConstructor
public enum MessageLevelEnum implements ParamEnum {

    /**
     * 重要
     */
    IMPORTANT_MSG("0", "重要"),

    /**
     * 一般
     */
    NORMAL_MSG("1", "一般"),

    /**
     * 不重要
     */
    NOT_IMPORTANT_MSG("2", "不重要");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

//    @JsonCreator
//    public static TaskPriorityEnum decode(final String value) {
//        return Stream.of(TaskPriorityEnum.values()).filter(targetEnum -> targetEnum.value.equals(value)).findFirst().orElse(null);
//    }

    /**
     * 根据类型的值，返回类型的枚举实例
     *
     * @param value
     * @return
     */
    public static MessageLevelEnum getEntityByVal(String value) {
        for (MessageLevelEnum entity : MessageLevelEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
