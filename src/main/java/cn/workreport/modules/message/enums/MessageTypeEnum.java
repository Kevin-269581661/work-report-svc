package cn.workreport.modules.message.enums;

import cn.workreport.modules.common.enums.ParamEnum;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息类型
 */
@AllArgsConstructor
public enum MessageTypeEnum implements ParamEnum {

    /**
     * 其他消息
     */
    OTHER_MSG("0", "其他消息"),

    /**
     * 系统消息
     */
    SYSTEM_MSG("1", "系统消息"),

    /**
     * 日报消息
     */
    DAY_REPORT_MSG("2", "日报消息"),

    /**
     * 周报消息
     */
    WEEK_REPORT_MSG("3", "周报消息"),

    /**
     * 个人消息
     * （用户信息发生了改变，角色，权限等，前端收到后需要重新获取用户信息）
     */
    PERSONAL_MSG("4", "个人消息");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

//    @JsonCreator
//    public static TaskPriorityEnum decode(final String value) {
//        return Stream.of(TaskPriorityEnum.values()).filter(targetEnum -> targetEnum.value.equals(value)).findFirst().orElse(null);
//    }

    /**
     * 根据类型的值，返回类型的枚举实例
     *
     * @param value
     * @return
     */
    public static MessageTypeEnum getEntityByVal(String value) {
        for (MessageTypeEnum entity : MessageTypeEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
