package cn.workreport.modules.message.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.common.annotations.PassToken;
import cn.workreport.modules.common.annotations.UserLoginToken;
import cn.workreport.modules.common.controller.BaseController;
import cn.workreport.modules.common.po.IdsPO;
import cn.workreport.modules.common.po.PagePO;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.modules.message.entity.Message;
import cn.workreport.modules.message.service.IMessageService;
import cn.workreport.modules.users.entity.UserEntity;
import cn.workreport.util.JsonResult;
import cn.workreport.util.PageQueryUtil;
import cn.workreport.util.UserChacheFromToken;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yyf
 * @since 2022-02-19
 */
@Slf4j
@RestController
@UserLoginToken
@Api(tags = "消息模块")
public class MessageController extends BaseController {

    @Autowired
    private IMessageService messageService;

    /**
     * 保存或修改
     *
     * @param entity 实体信息
     * @return 成功与否，成功返回实体信息，失败返回错误信息
     */
    @ApiOperation("修改或保存")
    @PostMapping("/message/saveOrUpdate")
    public JsonResult<?> saveOrUpdate(Message entity) {
        log.info("===> 进入 /message/saveOrUpdate ===> params = " + entity);
        return messageService.saveOrUpdateEntity(entity);
    }

    /**
     * 分页查询
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询")
    @PostMapping("/message/listPage")
    public JsonResult<?> listPage(PagePO<Message> pagePO) {
        log.info("===> 进入 /message/listPage ===> params = " + pagePO);
        IPage<Message> page = new PageQueryUtil<Message>().getPage(pagePO);
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Message::getCreatedTime);
        PageVO<Message> result = messageService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 根据ID查询
     * 如果查询不到，new 一个返回，可以作为新建请求
     *
     * @param id ID
     * @return 查询结果
     */
    @ApiOperation("根据ID查询")
    @GetMapping("/message/getById")
    public JsonResult<?> getById(Integer id) {
        log.info("===> 进入 /message/getById ===> params: id" + id);
        if (ObjectUtils.isEmpty(id)) {
            return JsonResult.fail("参数 id 不能为空！");
        }
        Message entity = messageService.getByIdEntity(id);
        return JsonResult.ok(entity);
    }

    /**
     * 批量或单体删除
     *
     * @param idsPO ID集合
     * @return 成功与否
     */
    @ApiOperation("批量删除")
    @PostMapping("/message/removeByIds")
    public JsonResult<?> removeByIds(IdsPO idsPO) {
        log.info("===> 进入 /message/removeByIds ===> params = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("ids 不能为空！");
        }
        return messageService.removeByIdsEntity(idsPO.getIds());
    }

    /**
     * 预加载数据
     * @return
     */
    // @ApiOperation("预加载数据")
    // @GetMapping("/message/preparedData")
    // public JsonResult<?> preparedData() {
    //   return messageService.preparedData();
    // }

    /**
     * 分页查询当前登录用户的消息
     *
     * @param pagePO
     * @return 查询结果
     */
    @ApiOperation("分页查询当前登录用户的消息")
    @PostMapping("/message/listPageSelf")
    public JsonResult<?> listPageSelf(@RequestBody PagePO<Message> pagePO) {
        log.info("===> 进入 /message/listPageSelf ===> params = " + pagePO);
        UserEntity currentLoginUser = UserChacheFromToken.getUser();
        IPage<Message> page = new PageQueryUtil<Message>().getPage(pagePO);
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<>();
        Message condition = pagePO.getCondition();
        if (!ObjectUtil.isEmpty(condition)) {
            log.info("isRead ===>" + condition.getIsRead());
            wrapper.eq(!ObjectUtil.isEmpty(condition.getIsRead()), Message::getIsRead, condition.getIsRead());
        }
        wrapper.and((wrapperInner) -> {
            wrapperInner.eq(Message::getReceiveUserId, currentLoginUser.getId());
            wrapperInner.or();
            wrapperInner.isNull(Message::getReceiveUserId);
        });
        wrapper.orderByDesc(Message::getCreatedTime);
        PageVO<Message> result = messageService.pageEntity(page, wrapper);
        return JsonResult.ok(result);
    }

    /**
     * 批量设置消息已读
     */
    @ApiOperation("批量设置消息已读")
    @PostMapping("/message/setHasReadByIds")
    public JsonResult<?> setHasReadByIds(IdsPO idsPO) {
        log.info("===> 进入 /message/setHasReadByIds ===> params = " + idsPO);
        if (ObjectUtils.isEmpty(idsPO.getIds())) {
            return JsonResult.fail("缺少参数 ids ");
        }
        return messageService.setHasReadByIds(idsPO.getIds());
    }

    /**
     * 获取当前登录用户的未读消息数量
     */
    @ApiOperation("获取当前登录用户的未读消息数量")
    @GetMapping("/message/getUnReadCount")
    public JsonResult<?> getUnReadCount() {
        log.info("===> 进入 /message/getUnReadCount ===>");
        return messageService.getUnReadCount();
    }

    /**
     * 给用户发送日报提醒
     */
    @ApiOperation("给用户发送日报提醒")
    @PostMapping("/message/sendReportNoticeToUser")
    public JsonResult<?> sendReportNoticeToUser(Integer userId) {
        log.info("===> 进入 /message/sendReportNoticeToUser ===> params: userId=" + userId);
        return messageService.sendReportNoticeToUser(userId);
    }

}
