package cn.workreport.modules.message.mapper;

import cn.workreport.modules.message.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-19
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

}
