package cn.workreport.modules.message.service;

import cn.workreport.modules.common.service.BaseService;
import cn.workreport.modules.message.entity.Message;
import cn.workreport.util.JsonResult;

import java.util.List;

/**
 * <p>
 *  业务层接口
 * </p>
 *
 * @author yyf
 * @since 2022-02-19
 */
public interface IMessageService extends BaseService<Message> {

    /**
     * 批量设置消息已读
     * @param ids 消息id集合
     * @return
     */
    JsonResult<?> setHasReadByIds(List<Integer> ids);

    /**
     * 获取当前登录用户的未读消息数量
     * @return
     */
    JsonResult<?> getUnReadCount();

    /**
     * 给用户发送日报提醒
     *
     * @param userId
     * @return
     */
    JsonResult<?> sendReportNoticeToUser(Integer userId);

    /**
     * 发送消息给指定用户
     * @param userId 用户 id
     * @param message 消息类
     */
    boolean sendMsgToUser(Integer userId, Message message);

    /**
     * 发送消息给指定部分用户
     * @param userIds 用户 id 集合
     * @param message 消息类
     */
    boolean sendMsgToSomeUser(List<Integer> userIds, Message message);

    /**
     * 给所有人发送消息
     * @param message 消息类
     */
    boolean sendMsgToAll(Message message);
}

