package cn.workreport.modules.common.controller;

import cn.workreport.modules.common.exception.HasNoPermissionException;
import cn.workreport.modules.common.exception.TokenAuthExpiredException;
import cn.workreport.util.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 控制器的基类
 * 可以在这里做全局的异常处理（兼容SpringMVC框架）
 * 必须是 public 权限，否则框架不识别
 */
public abstract class BaseController {

    /**
     * 统一处理异常的方法
     * 框架自动try catch 调用controller的方法，捕获到错误，则调用此处理方法
     * @param e 错误类
     * @return 返回值跟controller设计一样
     * @ExceptionHandler 的参数限制处理异常的种类（只处理参数类型的异常）
     * @ExceptionHandler 的参数是一个数组，可以写多个异常种类
     */
    @ExceptionHandler()
    public JsonResult<Void> handleException(Throwable e) {
        JsonResult<Void> jr = new JsonResult<>(e);
        System.err.println("handleException: " + e.getClass());
        if (e instanceof TokenAuthExpiredException) {
            jr.setState("401");
            jr.setMessage(e.getMessage());
        }
        else if (e instanceof HasNoPermissionException) {
            jr.setState("403");
            jr.setMessage(e.getMessage());
        }
        else {
            jr.setState("500");
            jr.setMessage(e.getMessage());
        }
        return jr;
    }

}
