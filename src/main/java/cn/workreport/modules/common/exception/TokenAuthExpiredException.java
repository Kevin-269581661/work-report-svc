package cn.workreport.modules.common.exception;

public class TokenAuthExpiredException extends RuntimeException {
    public TokenAuthExpiredException() {
        super();
    }

    public TokenAuthExpiredException(String message) {
        super(message);
    }

    public TokenAuthExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenAuthExpiredException(Throwable cause) {
        super(cause);
    }

    protected TokenAuthExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
