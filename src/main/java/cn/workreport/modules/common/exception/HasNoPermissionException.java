package cn.workreport.modules.common.exception;

public class HasNoPermissionException extends RuntimeException {
    public HasNoPermissionException() {
        super();
    }

    public HasNoPermissionException(String message) {
        super(message);
    }

    public HasNoPermissionException(String message, Throwable cause) {
        super(message, cause);
    }

    public HasNoPermissionException(Throwable cause) {
        super(cause);
    }

    protected HasNoPermissionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
