package cn.workreport.modules.common.exception;

/**
 * 业务异常的基类
 * 继承自 RuntimeException
 * 需要添加 serialVersionUID （因为 RuntimeException 继承了序列化类 ）
 * 添加 RuntimeException 所有的构造方法
 */
public class ServiceException extends RuntimeException{
    private static final long serialVersionUID = -7323310431109356566L;

    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    protected ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
