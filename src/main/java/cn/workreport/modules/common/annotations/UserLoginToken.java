package cn.workreport.modules.common.annotations;

import cn.workreport.modules.common.enums.UserRoleEnum;
import cn.workreport.modules.permission.enums.PermissionEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserLoginToken {

    /**
     * 是否开启token验证 （默认开启）
     * @return
     */
    boolean required() default true;

    /**
     * 用户角色权限 （默认普通用户）
     * @return
     */
     PermissionEnum[] permission() default {};
}
