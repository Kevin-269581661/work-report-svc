package cn.workreport.modules.common.annotations;

import java.lang.annotation.*;

/**
 * 描述【】
 *
 * @author lihuanyao
 * @time 2022/2/23 17:46
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyToDoing {

    boolean doing() default true;

    boolean rename() default false;

    String renameValue() default "";
}
