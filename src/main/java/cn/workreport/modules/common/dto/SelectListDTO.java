package cn.workreport.modules.common.dto;

import lombok.Data;

@Data
public class SelectListDTO {

    private String label;

    private String val;

}
