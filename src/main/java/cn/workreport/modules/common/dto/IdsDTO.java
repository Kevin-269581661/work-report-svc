package cn.workreport.modules.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class IdsDTO {
    private List<Integer> ids;
}
