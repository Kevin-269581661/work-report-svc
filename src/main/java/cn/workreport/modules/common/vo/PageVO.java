/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package cn.workreport.modules.common.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 */
@Data
public class PageVO<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 总记录数
	 */
	private int total;
	/**
	 * 每页记录数
	 */
	private int size;
	/**
	 * 总页数
	 */
	private int pages;
	/**
	 * 当前页数
	 */
	private int current;
	/**
	 * 列表数据
	 */
	private List<T> list;
	
	/**
	 * 分页
	 * @param list        列表数据
	 * @param total  总记录数
	 * @param size    每页记录数
	 * @param current    当前页数
	 */
	public PageVO(List<T> list, int total, int size, int current) {
		this.list = list;
		this.total = total;
		this.size = size;
		this.current = current;
		this.pages = (int) Math.ceil( (double) total / size);
	}

	/**
	 * 分页
	 */
	public PageVO(IPage<T> page) {
		this.list = page.getRecords();
		this.total = (int)page.getTotal();
		this.size = (int)page.getSize();
		this.current = (int)page.getCurrent();
		this.pages = (int)page.getPages();
	}
}
