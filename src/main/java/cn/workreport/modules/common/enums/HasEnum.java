package cn.workreport.modules.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用枚举 ： “有” 或 “无”
 */
@AllArgsConstructor
public enum HasEnum implements ParamEnum {

    /**
     * 有
     */
    YES("1", "有"),

    /**
     * 无
     */
    NO("0", "无");

    // 前端需要传递枚举类属性的值
    @JsonValue
    // 数据库字段值
    @EnumValue
    @Getter
    private String value;

    @Getter
    private String label;

    /**
     * 根据类型的值，返回类型的枚举实例
     * @param value
     * @return
     */
    public static HasEnum getEntityByVal (String value) {
        for (HasEnum entity : HasEnum.values()) {
            if (entity.getValue().equals(value)) {
                return entity;
            }
        }
        return null;
    }
}
