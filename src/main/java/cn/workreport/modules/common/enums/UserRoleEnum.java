package cn.workreport.modules.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色枚举
 */

@AllArgsConstructor
public enum UserRoleEnum {

    /**
     * 超级管理员
     */
    ADMIN(9999,"超级管理员"),

    /**
     * 管理员
     */
    MANAGER(10,"管理员"),

    /**
     * 普通用户
     */
    NORMAL(0,"普通用户");


    // 数据库字段值
    @EnumValue
    @Getter
    private Integer roleType;

    // 前端需要传递枚举类属性的值
    @JsonValue
    @Getter
    private final String description;

    /**
     * 根据类型的名称，返回类型的枚举实例。
     * @param roleType
     * @return
     */
    public static UserRoleEnum getRoleEntity(Integer roleType) {
        for (UserRoleEnum role : UserRoleEnum.values()) {
            if (role.getRoleType().equals(roleType)) {
                return role;
            }
        }
        return null;
    }

}
