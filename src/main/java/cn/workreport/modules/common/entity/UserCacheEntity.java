package cn.workreport.modules.common.entity;

import cn.workreport.modules.role.entity.Role;
import cn.workreport.modules.users.entity.UserEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserCacheEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private String token;

    private UserEntity user;

    private Role userRole;
}
