package cn.workreport.modules.common.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * token 中存储的数据
 */
@Data
public class TokenParseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer userId;

    private Long startTime;
}
