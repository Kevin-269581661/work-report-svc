package cn.workreport.modules.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体类的基类
 * 默认权限：其他包不能直接访问（类权限：要么publice ，要么默认）
 * 抽象类：不能实例化，只能继承（或者封装一些静态方法）
 * Serializable 接口：序列化接口（里面没有方法）
 * 如果一个类没有功能方法（除了get/set方法），主要描述的是一些属性，那就应该实现序列化接口！
 */
@Data
public abstract class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    protected Integer id; // "主键id"

    @TableField(fill = FieldFill.INSERT)
    protected Integer creatorId; // "创建人id"

    @TableField(fill = FieldFill.INSERT)
    protected String creatorName; // "创建人名"

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createdTime; // "创建时间"

    @TableField(fill = FieldFill.UPDATE)
    protected Integer modifierId; // "最后修改人id"

    @TableField(fill = FieldFill.UPDATE)
    protected String modifierName; // "最后修改人名"

    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date modifiedTime; // "最后修改时间"
}
