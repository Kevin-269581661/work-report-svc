package cn.workreport.modules.common.po;

import lombok.Data;

import java.util.List;

@Data
public class IdsPO {
    private List<Integer> ids;
}
