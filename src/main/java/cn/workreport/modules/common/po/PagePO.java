package cn.workreport.modules.common.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Data
public class PagePO<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页数（不传默认为 1）
     */
    private Long current;

    /**
     * 当前页大小（不传默认为 查所有）
     */
    private Long size;

    /**
     * 排序字段（不传默认为创建时间）
     */
    private String orderField;

    /**
     * 是否是降序 （默认是升序）
     */
    private Boolean asDesc;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startDateTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endDateTime;

    private T condition;
}
