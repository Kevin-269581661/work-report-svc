package cn.workreport.modules.common.service;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.common.exception.ServiceException;
import cn.workreport.modules.common.vo.PageVO;
import cn.workreport.util.CommonUtil;
import cn.workreport.util.JsonResult;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 基础 Service
 */
public interface BaseService<T> extends IService<T> {

    /**
     * 数据查询到之后，覆盖此方法，可以对查询到的数据进行处理再给前端显示
     */
    void wrapEntity(T entity);

    /**
     * 分页查询
     * 查询到数据，for循环自动调用wrapEntity方法。
     */
    PageVO<T> pageEntity(IPage<T> page, Wrapper<T> queryWrapper);

    /**
     * 抽取到数据之后，会自动调用wrapEntity
     */
    T getByIdEntity(Integer id);

    /**
     * 根据是否有id，自动保存或者更新（
     */
    JsonResult<?> saveOrUpdateEntity(T entity);

    /**
     * 底层调用removeByIdsEntity
     * 覆盖此方法，可以在数据删除之前做其他额外动作
     */
    JsonResult<?> removeByIdEntity(Integer id);

    /**
     * 批量删除，循环调用removeByIdEntity
     */
    JsonResult<?> removeByIdsEntity(List<Integer> ids);

    default boolean updateByCondition(LambdaUpdateWrapper<T> updateWrapper) {
        if (updateWrapper.isEmptyOfNormal()) {
            throw new ServiceException("缺少修改条件！");
        }
        return update(updateWrapper);
    }

}
