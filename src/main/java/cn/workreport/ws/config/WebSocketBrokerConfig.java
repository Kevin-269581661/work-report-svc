package cn.workreport.ws.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketBrokerConfig implements WebSocketMessageBrokerConfigurer {


    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // 路径 "/ws" 被注册为 STOMP 端点，对外暴露，客户端通过该路径接入WebSocket服务
        registry.addEndpoint("/ws").setAllowedOrigins("*").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 用户可以订阅来自以"/topic"为前缀的消息，客户端只可以订阅这个前缀的主题
        registry.enableSimpleBroker("/queue", "/topic", "/user");
        // 指定用户发送（一对一）的主题前缀是“/user/”
        registry.setUserDestinationPrefix("/user");
        // 客户端发送过来的消息，需要以"/app"为前缀，再经过 Broker 转发给响应的 Controller
        registry.setApplicationDestinationPrefixes("/app");
    }
}
