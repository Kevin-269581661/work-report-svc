package cn.workreport.ws.job;

import cn.workreport.ws.service.WsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * 消息推送job
 */

@Slf4j
@Component
public class MessageJob {

    @Autowired
    private WsService wsService;

    public void run () {
        try {
            wsService.broadcastMsg("自动生成消息广播：" + LocalDateTime.now().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
