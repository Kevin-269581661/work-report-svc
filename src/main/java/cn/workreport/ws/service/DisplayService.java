package cn.workreport.ws.service;

import cn.hutool.core.bean.BeanUtil;
import cn.workreport.modules.message.entity.Message;
import cn.workreport.modules.message.enums.MessageTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class DisplayService {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    public void showWelcome (String code, Message messageEntity) {
        Message msgEntity = new Message();
        BeanUtil.copyProperties(messageEntity, msgEntity);
        msgEntity.setType(MessageTypeEnum.DAY_REPORT_MSG);
        // 调用convertAndSendToUser来推送消息
        simpMessagingTemplate.convertAndSendToUser(code, "/queue/info", msgEntity);
    }

    public void showAllMsg (String code) {
        Message msgEntity = new Message();
        msgEntity.setType(MessageTypeEnum.OTHER_MSG);
        // 调用convertAndSendToUser来推送消息
        simpMessagingTemplate.convertAndSendToUser(code, "/queue/info", msgEntity);
    }
}
