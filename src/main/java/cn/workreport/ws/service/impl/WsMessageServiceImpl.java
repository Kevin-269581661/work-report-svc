package cn.workreport.ws.service.impl;

import cn.workreport.modules.message.service.impl.MessageServiceImpl;
import cn.workreport.ws.service.WsMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WsMessageServiceImpl implements WsMessageService {

    @Autowired
    private MessageServiceImpl messageService;

}
