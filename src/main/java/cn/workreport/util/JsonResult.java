package cn.workreport.util;

import lombok.Data;

@Data
public class JsonResult<T> {

    public final static String FAIL = "0001";
    public final static String SUCCESS = "0000";
    private String state;
    private String message;
    private T data;

    // 保留无参构造方法
    public JsonResult() { }

    public JsonResult(Throwable e) {
        this.message = e.getMessage();
    }

    public JsonResult(String state) {
        this.state = state;
    }

    public static <T> JsonResult<T> ok(T data) {
        return new JsonResult<T>(SUCCESS, data);
    }

    public static <T> JsonResult<T> ok() {
        return new JsonResult<T>(SUCCESS, "操作成功");
    }

    public static <T> JsonResult<T> fail(String state, T data) {
        return new JsonResult<T>(state, data);
    }

    public static <T> JsonResult<T> fail(String message) {
        return new JsonResult<T>(FAIL, message);
    }

    public static <T> JsonResult<T> fail() {
        return new JsonResult<T>(FAIL, "操作失败");
    }

    public JsonResult(String state, String message) {
        this.state = state;
        this.message = message;
    }

    public JsonResult(String state, T data) {
        this.state = state;
        this.message = "操作成功";
        this.data = data;
    }
}

