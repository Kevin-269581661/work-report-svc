package cn.workreport.util;

import cn.workreport.modules.common.enums.ParamEnum;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.workreport.util.CommonUtil.firstLetterToUpper;

@Slf4j
public class EnumUtil<T> {

    /**
     * 把枚举类实例转成列表数据
     *
     * @param className 包名 + 类名
     * @return
     * @throws Exception
     */
    public static List<Map<String, Object>> getSelectList(String className) {
        List<Map<String, Object>> selectList = new ArrayList<>();
        // 1.获得枚举类对象
        try {
            Class<Enum> clz = (Class<Enum>) Class.forName(className);
            // 2.获得全部枚举常量
            Object[] objects = clz.getEnumConstants();
            Method getLabel = clz.getMethod("getLabel");
            Method getValue = clz.getMethod("getValue");
            for (Object obj : objects) {
                Map<String, Object> map = new HashMap<>();
                map.put("label", getLabel.invoke(obj));
                map.put("value", getValue.invoke(obj));
                selectList.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(className + "解析出错！");
        }
        return selectList;
    }

    public static List<Map<String, Object>> getSelectList(Class<? extends ParamEnum> clz) {
        List<Map<String, Object>> selectList = new ArrayList<>();
        // 1.获得枚举类对象
        try {
            // 2.获得全部枚举常量
            Object[] objects = clz.getEnumConstants();
            Method getLabel = clz.getMethod("getLabel");
            Method getValue = clz.getMethod("getValue");
            for (Object obj : objects) {
                Map<String, Object> map = new HashMap<>();
                map.put("label", getLabel.invoke(obj));
                map.put("value", getValue.invoke(obj));
                selectList.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("getSelectList 解析出错！");
        }
        return selectList;
    }

    public static List<Map<String, Object>> getSelectList(String className, String labelPropName, String valPropName) {
        List<Map<String, Object>> selectList = new ArrayList<>();
        try {
            Class<Enum> clz = (Class<Enum>) Class.forName(className);
            Object[] objects = clz.getEnumConstants();
            String labelMethodName = "get" + firstLetterToUpper(labelPropName);
            String valMethodName = "get" + firstLetterToUpper(valPropName);
            Method getLabel = clz.getMethod(labelMethodName);
            Method getValue = clz.getMethod(valMethodName);
            for (Object obj : objects) {
                Map<String, Object> map = new HashMap<>();
                map.put("label", getLabel.invoke(obj));
                map.put("value", getValue.invoke(obj));
                selectList.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("解析出错！");
        }
        return selectList;
    }


}
