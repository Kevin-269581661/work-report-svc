package cn.workreport.util;

import cn.hutool.core.util.ObjectUtil;
import cn.workreport.modules.permission.enums.PermissionEnum;
import cn.workreport.modules.role.entity.Role;
import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 初始化数据（用于数据库初始化所必须的一些数据，一般情况下不可缺少该数据）
 */
@Component
public class InitData {

    /**
     * 初始化的角色
     */
    @AllArgsConstructor
    public enum InitRoleNameEnum {
        /**
         * 普通用户
         */
        NORMAL_ROLE("普通用户"),
        /**
         * 组织管理者
         */
        GROUP_MANAGER("组织管理者"),
        /**
         * 机构管理者
         */
        ORG_MANAGER("机构管理者");

        @EnumValue
        @Getter
        private String roleName;
    }

    /**
     * 获取初始化角色的列表
     *
     * @param orgId 机构id
     * @return 初始化角色的列表
     */
    public List<Role> getInitRoleList(Integer orgId) {
        if (ObjectUtil.isEmpty(orgId)) {
            return null;
        }
        List<Role> roleList = new ArrayList<>();
//        PermissionEnum normalRolePermissions[] = new PermissionEnum[] {
//            PermissionEnum.TASK_PAGE,
//                PermissionEnum.RELEASE_PAGE,
//                PermissionEnum.REPORT_PAGE,
//                PermissionEnum.REPORT_SAVE_OR_UPDATE,
//                PermissionEnum.REPORT_DELETE,
//                PermissionEnum.REPORT_EXPORT,
//                PermissionEnum.REPORT_SEND,
//                PermissionEnum.REPORT_COMMIT_PAGE,
//                PermissionEnum.REPORT_COMMIT_DELETE,
//                PermissionEnum.GROUP_PAGE,
//                PermissionEnum.GROUP_REPORT_TREE,
//                PermissionEnum.CASCADE_PAGE,
//                PermissionEnum.CASCADE_SAVE_OR_UPDATE
//        };
        EnumSet<PermissionEnum> normalRolePermissions = EnumSet.of(
                PermissionEnum.TASK_PAGE,
                PermissionEnum.RELEASE_PAGE,
                PermissionEnum.REPORT_PAGE,
                PermissionEnum.REPORT_SAVE_OR_UPDATE,
                PermissionEnum.REPORT_DELETE,
                PermissionEnum.REPORT_EXPORT,
                PermissionEnum.REPORT_SEND,
                PermissionEnum.REPORT_COMMIT_PAGE,
                PermissionEnum.REPORT_COMMIT_DELETE,
                PermissionEnum.GROUP_PAGE,
                PermissionEnum.GROUP_REPORT_TREE,
                PermissionEnum.CASCADE_PAGE,
                PermissionEnum.CASCADE_SAVE_OR_UPDATE
        );
        EnumSet<PermissionEnum> groupManagerExtendsPermissions = EnumSet.of(
                PermissionEnum.TASK_SAVE_OR_UPDATE,
                PermissionEnum.TASK_DELETE,
                PermissionEnum.RELEASE_SAVE_OR_UPDATE,
                PermissionEnum.RELEASE_DELETE,
                PermissionEnum.CASCADE_DELETE
        );
        EnumSet<PermissionEnum> orgManagerExtendsPermissions = EnumSet.of(
                PermissionEnum.USERS_PAGE,
                PermissionEnum.USERS_UPDATE_GROUP,
                PermissionEnum.USERS_UPDATE_BY_ADMIN,
                PermissionEnum.USERS_UPDATE_ROLE_BY_ADMIN,
                PermissionEnum.USERS_UPDATE_PW_BY_ADMIN,
                PermissionEnum.USERS_UPDATE_STATUS_BY_ADMIN,
                PermissionEnum.RELEASE_TOGGLE_ROOF,
                PermissionEnum.ROLE_PAGE,
                PermissionEnum.ROLE_SAVE_OR_UPDATE,
                PermissionEnum.ROLE_GET_BY_ID,
                PermissionEnum.ROLE_DELETE,
                PermissionEnum.ROLE_TOGGLE_USE,
                PermissionEnum.PERMISSION_PAGE,
                PermissionEnum.PERMISSION_SAVE_OR_UPDATE,
                PermissionEnum.PERMISSION_DELETE,
                PermissionEnum.GROUP_DELETE,
                PermissionEnum.GROUP_SAVE_OR_UPDATE
        );

        EnumSet<PermissionEnum> groupManagerPermissions = EnumSet.copyOf(normalRolePermissions);
        groupManagerPermissions.addAll(groupManagerExtendsPermissions);

        EnumSet<PermissionEnum> orgManagerPermissions = EnumSet.copyOf(groupManagerPermissions);
        orgManagerPermissions.addAll(orgManagerExtendsPermissions);

        Role normalRole = new Role();
        normalRole.setRoleName(InitRoleNameEnum.NORMAL_ROLE.getRoleName());
        normalRole.setOrgId(orgId);
        normalRole.setPermissions(normalRolePermissions.stream().map(item -> item.getValue()).collect(Collectors.toList()));
        normalRole.setAsUse(true);

        Role groupManagerRole = new Role();
        groupManagerRole.setRoleName(InitRoleNameEnum.GROUP_MANAGER.getRoleName());
        groupManagerRole.setOrgId(orgId);
        groupManagerRole.setPermissions(groupManagerPermissions.stream().map(item -> item.getValue()).collect(Collectors.toList()));
        groupManagerRole.setAsUse(true);

        Role orgManagerRole = new Role();
        orgManagerRole.setRoleName(InitRoleNameEnum.ORG_MANAGER.getRoleName());
        orgManagerRole.setOrgId(orgId);
        orgManagerRole.setPermissions(orgManagerPermissions.stream().map(item -> item.getValue()).collect(Collectors.toList()));
        orgManagerRole.setAsUse(true);

        roleList.add(normalRole);
        roleList.add(groupManagerRole);
        roleList.add(orgManagerRole);
        return roleList;
    }

}
