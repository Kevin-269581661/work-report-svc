package cn.workreport.util;

public class CommonUtil {

    /**
     * 首字母转大写
     * @param letter
     * @return
     */
    public static String firstLetterToUpper (String letter) {
        char[] chars = letter.toCharArray();
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            chars[0] = (char) (chars[0] - 32);
        }
        return new String(chars);
    }

    public static String firstLetterToUpper2 (String letter) {
        return letter.substring(0, 1).toUpperCase() + letter.substring(1);
    }
}
