package cn.workreport.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.workreport.modules.common.annotations.MyToDoing;
import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述【】
 *
 * @author lihuanyao
 * @time 2022/2/23 17:41
 */
public class GenerateUtil {

    /**
     * 转化为json
     *
     * @param aClass
     */
    public static void toJsonDoing(Class aClass) {
        Field[] fields = aClass.getDeclaredFields();
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("{");
        for (Field field : fields) {
            field.setAccessible(true);
            MyToDoing annotation = field.getAnnotation(MyToDoing.class);
            // 只有存在标识的才进行处理
            if (annotation != null && annotation.doing()) {
                if (field.getType() == String.class) {
                    String name = field.getName();
                    name = name.toLowerCase();
                    if (name.contains("id")) {
                        strBuff.append("\"" + field.getName() + "\" : \"" + UUID.randomUUID().toString().replace("-", "") + "\"");
                    } else {
                        strBuff.append("\"" + field.getName() + "\" : \"" + "其他数据" + "\"");
                    }
                    strBuff.append(",");
                } else if (field.getType().isEnum()) {
                    Class<Enum> type = (Class<Enum>) field.getType();
                    Field[] declaredFields = type.getDeclaredFields();
                    Enum anEnum = Enum.valueOf(type, declaredFields[0].getName());
                    Class<? extends Enum> aClass1 = anEnum.getClass();
                    try {
                        Method method = aClass1.getMethod("getDescription");
                        strBuff.append("\"" + field.getName() + "\" : \"" + method.invoke(anEnum) + "\"");
                        strBuff.append(",");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (field.getType() == Date.class) {
                    strBuff.append("\"" + field.getName() + "\" : \"" + DateUtil.format(new Date(), "yyyy-Mm-dd") + "\"");
                    strBuff.append(",");
                } else if (field.getType() == Integer.class) {
                    strBuff.append("\"" + field.getName() + "\" : \"" + (int) (Math.random() * 12 + 1) + "\"");
                    strBuff.append(",");
                } else if (field.getType() == Boolean.class) {
                    strBuff.append("\"" + field.getName() + "\" : " + false);
                    strBuff.append(",");
                }
            }
        }
        strBuff.append("}");
        System.out.println(strBuff.toString().replace(",}", "}"));
    }

    /**
     * 转化为xml
     *
     * @param aClass
     */
    public static void toXmlDoing(Class aClass, String paramStr) {
        if (StrUtil.isBlank(paramStr)) {
            paramStr = "";
        }
        Field[] fields = aClass.getDeclaredFields();
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" <where> \n");
        for (Field field : fields) {
            field.setAccessible(true);
            MyToDoing annotation = field.getAnnotation(MyToDoing.class);
            // 只有存在标识的才进行处理
            if (annotation != null && annotation.doing()) {
                String name = field.getName();
                String lable = paramStr + "." + name;
                // 转下划线
                name = humpToLine2(name);
                if (field.getType() == String.class) {
                    strBuff.append("  <if test=\"" + lable + " != null\">\n" +
                            "                AND info." + name + " LIKE CONCAT('%',#{" + lable + "},'%')\n" +
                            "            </if> \n");
                } else if (field.getType() == Date.class) {
                    String low = name.toLowerCase();
                    if (low.contains("min")) {
                        strBuff.append("    <if test=\"" + lable + " != null\">\n" +
                                "                    AND info." + name + " <![CDATA[ >= ]]> #{" + lable + "Min" + "}\n" +
                                "           </if>\n");
                    }
                    if (low.contains("max")) {
                        strBuff.append("           <if test=\"" + lable + " != null\">\n" +
                                "             AND info." + name + " <![CDATA[ <= ]]> #{" + lable + "}\n" +
                                "           </if> \n");
                    }

                    if (low.contains("start")) {
                        strBuff.append("    <if test=\"" + lable + " != null\">\n" +
                                "                    AND info." + name + " <![CDATA[ >= ]]> #{" + lable + "Min" + "}\n" +
                                "           </if>\n");
                    }
                    if (low.contains("end")) {
                        strBuff.append("           <if test=\"" + lable + " != null\">\n" +
                                "             AND info." + name + " <![CDATA[ <= ]]> #{" + lable + "}\n" +
                                "           </if> \n");
                    }
                } else {
                    strBuff.append("   <if test=\"" + lable + " != null\">\n" +
                            "                AND info." + name + " = #{" + lable + "}\n" +
                            "            </if> \n");
                }
            }
        }
        strBuff.append("\n </where>");
        System.out.println(strBuff.toString());
    }

    /**
     * 转化为xml
     *
     * @param aClass
     */
    public static void toExcelDoing(Class aClass) {
        Field[] fields = aClass.getDeclaredFields();
        StringBuffer strBuff = new StringBuffer();
        for (Field field : fields) {
            field.setAccessible(true);
            MyToDoing annotation = field.getAnnotation(MyToDoing.class);
            // 只有存在标识的才进行处理
            if (annotation != null && annotation.doing()) {
                String name = field.getName();
                if (name.toLowerCase().contains("id")) {
                    name += "Value";
                }
                ApiModelProperty annotation1 = field.getAnnotation(ApiModelProperty.class);
                strBuff.append("    @ExcelProperty(value = \"" + annotation1.value() + "\")\n" +
                        "    private String " + name + "; \n");
            }
        }
        System.out.println(strBuff.toString());
    }

    /**
     * 转化为xml
     *
     * @param aClass
     */
    public static void toSelectDoing(Class aClass) {
        Field[] fields = aClass.getDeclaredFields();
        StringBuffer strBuff = new StringBuffer();
        for (Field field : fields) {
            field.setAccessible(true);
            MyToDoing annotation = field.getAnnotation(MyToDoing.class);
            // 只有存在标识的才进行处理
            if (annotation != null && annotation.doing()) {
                String name = field.getName();
                String line = humpToLine2(name);
                if (annotation.rename()) {
                    line = annotation.renameValue();
                    strBuff.append(" info." + line + " as " + name);
                } else {
                    strBuff.append(" info." + line);
                }
                strBuff.append(", \n");
            }
        }
        System.out.println(strBuff.toString());
    }


    private static Pattern linePattern = Pattern.compile("_(\\w)");

    /**
     * 下划线转驼峰
     */
    private static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private static Pattern humpPattern = Pattern.compile("[A-Z]");

    /**
     * 驼峰转下划线,效率比上面高
     */
    private static String humpToLine2(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

}
