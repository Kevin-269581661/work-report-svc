package cn.workreport.config;

import org.springframework.stereotype.Component;

/**
 * AttributeConvertEnItemTypeype, String>. Implements the following methods :
 * convertToDatabaseColumn : (given an Enum returns a String) 将枚举转换为字符串，在插入或更新行时使用
 * convertToEntityAttribute : (given a String returns an Enum) 将来自数据库的数据(仅映射到具有EnItemType枚举的实体中的数据)转换为正确的枚举值
 */
//@Component
//@Converter(autoApply = true)
//public class EnItemTypeConverter implements AttributeConverter {
//
//    @Override
//    public String convertToDatabaseColumn(final EnItemType attribute) {
//        return Optional.ofNullable(attribute).map(EnItemType::getCode).orElse(null);
//    }
//
//    @Override
//    public EnItemType convertToEntityAttribute(final String dbData) {
//        return EnItemType.decode(dbData);
//    }
//
//}

