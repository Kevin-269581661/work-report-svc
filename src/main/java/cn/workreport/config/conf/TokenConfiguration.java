package cn.workreport.config.conf;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * token 配置类
 */
@Data
@Configuration
// prefix 属性：表示的是根节点的名称
@ConfigurationProperties(prefix = "token")
@Slf4j
public class TokenConfiguration {

    private String privateKey;

    // 需要刷新的时间（超过此时间，在过期时间范围内，会重置token的过期时间）
    private Long yangToken;

    // 过期时间（超过此时间，视为登录已失效）
    private Long oldToken;
}
