package cn.workreport.tests;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.workreport.modules.task.entity.ReleaseEntity;
import cn.workreport.util.GenerateUtil;
import cn.workreport.util.TokenUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@SpringBootTest
public class Tests {

    @Autowired
    private TokenUtil tokenUtil;

    @Test
    public void getPath() throws IOException {
        //当前项目下路径
        File file = new File("");
        String filePath = file.getCanonicalPath();
        System.out.println("当前项目下路径: " + filePath);

        //当前项目下xml文件夹
        File file1 = new File("");
        String filePath1 = file1.getCanonicalPath()+File.separator+"xml\\";
        System.out.println("当前项目下xml文件夹: " + filePath1);

        //获取类加载的根路径
        File file3 = new File(this.getClass().getResource("/").getPath());
        System.out.println("获取类加载的根路径: " + file3);

        //获取当前类的所在工程路径
        File file4 = new File(this.getClass().getResource("").getPath());
        System.out.println("获取当前类的所在工程路径: " + file4);

        //获取所有的类路径 包括jar包的路径
        System.out.println("获取所有的类路径 包括jar包的路径: " + System.getProperty("java.class.path"));
    }

    @Test
    public void testToken () {
        int userId = 12;
        String userRole = "admin";
//        String token = tokenUtil.createToken(userId);
//        System.out.println("token ==> " + token);
    }

    @Test
    public void checkToken () {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lU3RhbXAiOjE2MzY2NDQ0MjkzMDYsInVzZXJJZCI6N30.Aaprfp6zML3eD06zRAAg9F8rjhByNxojfILoosugEVg";
        String token2 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aW1lU3RhbXAiOjE2MzY2NDcxMjUwOTcsInVzZXJSb2xlIjoiYWRtaW4iLCJ1c2VySWQiOjEyfQ.IQtIQkkVjo5rdHFUq_Jb5TqkOK2h2eErYICKmaWN1VA";
    }

    @Test
    public void test () {
//        GenerateUtil.toJsonDoing(ReleaseEntity.class);
//        GenerateUtil.toXmlDoing(ReleaseEntity.class, "releaseName");
//        GenerateUtil.toExcelDoing(ReleaseEntity.class);
        GenerateUtil.toSelectDoing(ReleaseEntity.class);
        List list = new ArrayList(); // true
        List list2 = null; // true
        Integer number1 = 0; // false
        Integer number2 = null; // true
        int number3 = 0; // false
        String str1 = null; // true
        String str2 = ""; // true
        System.err.println("isEmpty ===> 1 " + ObjectUtil.isEmpty(str2));
        System.err.println("isEmpty ===> 2 " + ObjectUtils.isEmpty(str2));
//        System.err.println("isBlank ===> 1 " + StrUtil.isBlank(str1)); // true
//        System.err.println("isBlank ===> 2 " + StrUtil.isBlank(str2)); // true
    }
}
