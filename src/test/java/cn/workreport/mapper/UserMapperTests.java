package cn.workreport.mapper;

import cn.workreport.modules.users.entity.UserEntity;
import cn.workreport.modules.users.mapper.UserMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 加载Spring环境注解
 * @RunWith(SpringRunner.class)
 * @SpringBootTest
 * 这两行注解必须添加
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTests {

    @Autowired
    private UserMapper mapper;

    @Test
    public void insert () {
        UserEntity user = new UserEntity();
        user.setUsername("root33");
        user.setPassword("123456");

        System.err.println("user = " + user);
        Integer rows = mapper.insert(user);
        System.err.println("user = " + user);

        System.err.println("rows = " + rows);
    }

    @Test
    public void queryUsers () {
        Page<UserEntity> page = new Page<>();
        page.setCurrent(1);
        page.setSize(2);
        IPage<UserEntity> userList = mapper.queryUsers(page);
        System.err.println(userList);
    }

    @Test
    public void findByUserName () {
        String username = "root";
//        String username = "root6";
        UserEntity user = mapper.findByUserName(username);
        System.err.println("user = " + user);
    }
}
