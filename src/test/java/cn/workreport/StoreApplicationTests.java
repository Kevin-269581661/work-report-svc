package cn.workreport;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.util.DigestUtils;

import javax.sql.DataSource;
import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest
class StoreApplicationTests {
    @Autowired
    DataSource dataSource;

    @Test
    public void getConnection() throws SQLException {
        System.err.println(dataSource.getConnection());
    }

    @Test
    void contextLoads() {
    }

    /**
     * spring里内置的md5摘要算法
     */
//    @Test
//    public void md5() {
//        String password = "123456";
//        // 生成定长（32）的加密字符串
//        String md5Password = DigestUtils.md5DigestAsHex(password.getBytes());
//        System.err.println("md5Password = "+ md5Password);
//    }

    /**
     * 使用commons-codec里的加密算法（更强大）
     */
//    @Test
//    public void commonsCodec() {
//        String password = "123456";
//        password = DigestUtils.shaHex(password);
//        System.err.println("password = " + password);
//    }

}
