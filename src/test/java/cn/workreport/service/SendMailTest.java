package cn.workreport.service;

import cn.workreport.modules.mail.service.MailTemplate;
import cn.workreport.modules.mail.util.MailUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.junit4.SpringRunner;
import springfox.documentation.spring.web.json.Json;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述【】
 *
 * @author lihuanyao 模板
 * @date 2022/4/25 9:32
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SendMailTest {

    @Autowired
    private MailTemplate mailTemplate;



    @Test
    public void sendSimpleMail() {
//        String text = "以后就这么催日报了";
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setSubject("日报提醒");
//        message.setFrom("1551318033@qq.com");
//        message.setTo("1551318033@qq.com");
////        message.setCc("37xxxxx37@qq.com");
////        message.setBcc("14xxxxx098@qq.com");
//        message.setSentDate(new Date());
//        message.setText("<span style=\"color: red;\">以后就这么催日报了</span>");
//        javaMailSender.send(message);
//        HashMap<String, Object> hashMap = new HashMap<>();
//        String title2 = "文档.docx";
//        hashMap.put("文档.docx",new FileSystemResource(new File("E:\\YE\\BJYD-需求文档\\家签\\省家签系统需求规格说明书_10月优化功能_v1.2.docx")));
//        mailTemplate.sendImgMail("269581661@qq.com",
//                "日报提醒",
//                "<span style='color: red;'>以后就这么催日报了</span>" +
//                        "<p>给你一张图片：</p><img src='cid:zhufu'/>"+
//                        "<p>给你2张图片：</p><img src='cid:zhufu'/>",
//                hashMap
//        );
//        mailTemplate.sendAttachFileMail("269581661@qq.com",
//                "日报提醒",
//                "<span style='color: red;'>以后就这么催日报了</span>" +
//                        "<p>给你一张图片：</p><img src='cid:p01'/>",
//                hashMap
//        );
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.accumulate("username","李四");
//            jsonObject.accumulate("num","132");
//            jsonObject.accumulate("salary","120000");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("username","李四");
//        map.put("num","132");
//        map.put("salary","120000");
//        text = MailUtil.ObjectToTemplate(jsonObject, "dayReport.ftl");
//        text += "<p>map 结果如下<p/>" + MailUtil.ObjectToTemplate(map, "dayReport.ftl");
//        mailTemplate.sendTextMail("269581661@qq.com",
//                "日报提醒",
//                text
//        );

    }

    @Test
    public void sendTextMail() {
        Map<String, String> toMap = new HashMap<>();
        toMap.put("邱航星","297933634@qq.com");
//        toMap.put("宋启源","3078797258@qq.com");
//        toMap.put("叶云奉","269581661@qq.com");
        /**
         *  发送邮件
         */
        for (Map.Entry<String, String> entry : toMap.entrySet()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("username",entry.getKey());
            mailTemplate.sendTextMail(entry.getValue(),
                    "日报提醒", MailUtil.ObjectToTemplate(map, "dayReport.ftl")
            );
        }
    }
    @Test
    public void setMailTemplate(){
        ClassPathResource classPathResource = new ClassPathResource("static/images/report-imgs/ribao.jpg");
        HashMap<String, Object> hashMap = new HashMap<>();
        String imgName = "img-01";
        hashMap.put(
                imgName,
                classPathResource
        );
        Map<String, String> map = new HashMap<>();
        map.put("imgName",imgName);
        map.put("userName","李焕尧");
        String  content = MailUtil.ObjectToTemplate(map,"dayReport.ftl");
      mailTemplate.sendImgMail("1551318033@qq.com",
                "日报提醒",
                content,
                hashMap
        );
    }
}
