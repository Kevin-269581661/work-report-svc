package cn.workreport.service;

import cn.workreport.modules.users.po.UserPO;
import cn.workreport.modules.users.service.IUserService;
import cn.workreport.modules.common.exception.ServiceException;
import cn.workreport.util.JsonResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 加载Spring环境注解
 * @RunWith(SpringRunner.class)
 * @SpringBootTest
 * 这两行注解必须添加
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {

    @Autowired
    private IUserService userService;

    /**
     * 注册超级管理员
     */
    @Test
    public void regAdmin() {
        UserPO user = new UserPO();
        user.setUsername("root");
        user.setPassword("654321");
        user.setNickname("超级管理员");
        JsonResult jsonResult = userService.reg(user);
        System.out.println(jsonResult);
    }

    @Test
    public void reg() {
        try{
//            User user = new User();
//            user.setUsername("Tom22");
//            user.setPassword("123");
//            userService.reg(user);
            System.err.println("reg success!");
        }catch (ServiceException e) {
            System.err.println(e.getClass());
        }
    }

    @Test
    public void login() {
//        try{
//            String username = "Tom22";
//            String password = "123";
//            User user = userService.login(username, password);
//            System.err.println("login success! user = " + user);
//            System.err.println("login success! user = " + user);
//        }catch (ServiceException e) {
//            System.err.println(e.getClass());
//        }
    }

}
